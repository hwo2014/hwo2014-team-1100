import math
import track

def getQuadraticBezierPoint(p0, p1, p2, p3, t):
    tm = (1.0-t)
    return track.Point(tm*tm*tm*p0.x+3*tm*tm*t*p1.x+3*tm*t*t*p2.x+t*t*t*p3.x,
        tm*tm*tm*p0.y+3*tm*tm*t*p1.y+3*tm*t*t*p2.y+t*t*t*p3.y)

def getQuadraticBezierPointDerivate(p0, p1, p2, p3, t):
    tm = (1.0-t)
    return track.Point(3*tm*tm*(p1.x-p0.x) + 6*tm*t*(p2.x-p1.x)+3*t*t*(p3.x-p2.x),
    3*tm*tm*(p1.y-p0.y) + 6*tm*t*(p2.y-p1.y)+3*t*t*(p3.y-p2.y))

# creates lineCount straight lines and sums up their total length
def approxBezierLength(p0, p1, p2, p3):
    t = 0
    lineCount = 25.0
    prev = getQuadraticBezierPoint(track.Point(p0[0], p0[1]),
                                   track.Point(p1[0], p1[1]),
                                   track.Point(p2[0], p2[1]),
                                   track.Point(p3[0], p3[1]), t/lineCount)
    totalDist = 0.0
    for t in range(1,int(lineCount+1)):
        newP = getQuadraticBezierPoint(track.Point(p0[0], p0[1]),
                                       track.Point(p1[0], p1[1]),
                                       track.Point(p2[0], p2[1]),
                                       track.Point(p3[0], p3[1]), t/lineCount)
        dist = track.Point.distanceBetween(prev, newP)
        totalDist += dist
        prev = newP
    return totalDist


if __name__ == "__main__":

#index : 0 distance : -10 length : 100 start x: 137.849280 y: 253.015127 end x: 37.849280 y: 253.015127{0: 99.99999999999997, 1: 81.29016389298599}
#index : 1 distance : 10 length : 100 start x: 137.849280 y: 273.015127 end x: 37.849280 y: 273.015127{0: 81.29016389298602, 1: 100.0}

    l1start = (137.849280,253.015127)
    l1end = (37.849280,253.015127)
    l2start = (137.849280,273.015127)
    l2end = (37.849280, 273.015127)
    
    mid1 = ((l1start[0]+l1end[0])/2,(l1start[1]+l1end[1])/2)
    mid2 = ((l2start[0]+l2end[0])/2,(l2start[1]+l2end[1])/2)
    
    len = approxBezierLength(l1start, mid1, mid2, l2end)
    
    print(mid1)
    print(mid2)
    print(len)


