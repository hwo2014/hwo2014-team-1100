import json
import socket
import threading
import game

class NoobBot(threading.Thread):
    throttleCalibTicks = 100

    def __init__(self, socket, name, key, gamemodel=None, callback=None, trackName = 'keimola'):
        super(NoobBot, self).__init__()
        self.botRunning = threading.Semaphore(0)
        self.socket = socket
        self.name = name
        self.key = key
        self.gameModel = gamemodel
        self.needToSwitch = False
        self.lastSwitchIndex = -1
        self.nextIndexToSwitch = -1
        self.botStopCallback = callback
        self.haveThrottled = False
        self.currentThrottle = 0.6
        self.decelIdx = 0
        self.decelList = [0.5,0.4,0.3,0.2,0.1,0.0]
        self.accel = True
        self.trackName = trackName

        self.throttleLag = -1
        self.throttleConstant = 0.0
        self.throttleCalibData = []
        self.enoughAccelData = False
        self.enoughDecelData = False
        self.throttleCalibDecelTickStart = 0
        self.throttleCalibDecelSpeed = 0.0

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join_specific(self):
        return self.msg("joinRace", {"botId": { "name": self.name, "key": self.key}, "trackName": self.trackName, "carCount": 1})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def switch_lane(self, right):
        if right:
            print("Switching lane to right!")
            self.msg("switchLane","Right")
        else:
            print("Switching lane to left!")
            self.msg("switchLane","Left")

    def ping(self):
        self.msg("ping", {})

    def waitUntilBotDone(self):
        self.botRunning.acquire()

    def run(self):
        self.join_specific()
        self.msg_loop()
        self.botRunning.release()

    def on_join(self, data, extra):
        print("Joined %s"%(data))
        self.ping()

    def on_game_start(self, data, extra):
        print("Race started")
        self.ping()

    def on_car_positions(self, data, extra):
        canThrottle = True
        self.gameModel.updateGameTickInfo(data, extra)
        if self.gameModel.getTickCounter() > 0:
            currentPieceIndex = self.gameModel.getCurrentPieceIndex()
            if self.gameModel.isOnOptimalLane() == False and self.gameModel.isOnSwitchPiece() == False and currentPieceIndex > self.lastSwitchIndex:
                if self.needToSwitch == False:
                    self.needToSwitch = True
                    self.nextIndexToSwitch = self.gameModel.nextSwitchPieces()-1
                    print("Marking as a need to switch when reaching %d"%(self.nextIndexToSwitch))
                elif self.nextIndexToSwitch == currentPieceIndex:
                    print("Reached target block, trying to switch!")
                    canThrottle = False
                    self.lastSwitchIndex = currentPieceIndex
            elif self.gameModel.isOnOptimalLane() == True:
                nextSwitch = self.gameModel.nextSwitchPieces()
                if self.gameModel.laneStillOptimalAfterIndex(nextSwitch) == False and self.gameModel.isOnSwitchPiece() == False and currentPieceIndex > self.lastSwitchIndex:
                    if self.needToSwitch == False:
                        self.needToSwitch = True
                        self.nextIndexToSwitch = self.gameModel.nextSwitchPieces()-1
                        print("Marking as a need to switch when reaching %d (future) %d"%(self.nextIndexToSwitch, self.gameModel.getTickCounter()))
                    elif self.nextIndexToSwitch == currentPieceIndex:
                        print("Reached target block, trying to switch (future)! %d"%(self.haveThrottled))
                        canThrottle = False
                        self.lastSwitchIndex = currentPieceIndex

        if canThrottle:
            self.throttle(0.5)
            self.haveThrottled = True
        else:
            self.switch_lane(self.gameModel.switchToRight(self.lastSwitchIndex))
            self.needToSwitch = False

    # throttle speed time estimation tester, doesn't switch lanes
    def on_car_positions_time_est(self, data, extra):
        self.gameModel.updateGameTickInfo(data, extra)

        '''
        targetSpeedHigh = (0.6*10*60)
        targetSpeedLow = self.decelList[self.decelIdx%6]*10*60
        speedDiff = abs(targetSpeedHigh-self.gameModel.getCurrentCarSpeed())
        speedDiffLow = abs(targetSpeedLow-self.gameModel.getCurrentCarSpeed())
        if speedDiff < 2 and self.accel == True:
            self.currentThrottle = self.decelList[self.decelIdx%6]
            self.accel = False
            currentTick = self.gameModel.getTickCounter()
            targetSpeed = (self.currentThrottle*10.0*60.0+2.0)/60.0
            tickTarget = game.gameDataModel.ticksToReachVelDecel(currentTick, targetSpeed, self.gameModel.getCurrentCarSpeed()/60.0,self.currentThrottle)
            print("Decelarating with a throttle of %.3f, estimated tick to reach target speed: %d, current tick: %d!"%(self.currentThrottle, tickTarget, currentTick))
        if speedDiffLow < 2 and self.accel == False:
            self.currentThrottle = 0.6
            self.accel = True
            self.decelIdx += 1
            currentTick = self.gameModel.getTickCounter()
            targetSpeed = (self.currentThrottle*10.0*60.0-2.0)/60.0
            tickTarget = game.gameDataModel.ticksToReachVelAccel(self.gameModel.getTickCounter(), targetSpeed, self.gameModel.getCurrentCarSpeed()/60.0,self.currentThrottle)
            print("Accelerating with a throttle of %.3f, estimated tick to reach target speed: %d, current tick: %d!"%(self.currentThrottle, tickTarget, currentTick)) '''
        self.throttleCalibration()
        self.throttle(self.currentThrottle)

    def throttleCalibration(self):
        if self.enoughDecelData == True and self.enoughAccelData == True:
            return
        #print("Calib tick : %d, speed: %f"%(self.gameModel.getTickCounter(), self.gameModel.getCurrentCarSpeed()/60.0))
        if self.throttleLag == -1 and self.gameModel.getCurrentCarSpeed() > 0.0:
            self.throttleLag = self.gameModel.getTickCounter()

        if self.throttleLag >= 0:
            currentRealTick = (self.gameModel.getTickCounter()-self.throttleLag)
        else:
            currentRealTick = -1
        if currentRealTick >= 0 and currentRealTick < self.throttleCalibTicks:
            self.currentThrottle = 0.5
        elif currentRealTick >= self.throttleCalibTicks and currentRealTick < (2*self.throttleCalibTicks):
            self.currentThrottle = 0.0
            if self.enoughAccelData == False:
                self.throttleCalibDecelTickStart = self.gameModel.getTickCounter()
                self.throttleCalibDecelSpeed = self.gameModel.getCurrentCarSpeed()/60.0
                self.enoughAccelData = True
                self.gameModel.calculateAccelModelConstants(self.gameModel.getTickCounter(), self.gameModel.getCurrentCarSpeed()/60.0,
                        0.5, self.throttleCalibData, self.throttleLag)
        else:
            #print("Cunt : %d"%(currentRealTick))
            if self.enoughDecelData == False:
                self.enoughDecelData = True
                data = self.throttleCalibData[self.throttleCalibDecelTickStart:self.gameModel.getTickCounter()]
                #print(data)
                #print(self.throttleCalibData)
                #self.gameModel.calculateDecelModelConstants(self.throttleCalibDecelTickStart, self.gameModel.getTickCounter(),
                #    self.throttleCalibDecelSpeed, self.gameModel.getCurrentCarSpeed()/60.0, 0.0,
                #    data)
            self.currentThrottle = 0.5
        if currentRealTick >= 0 and currentRealTick < 2*self.throttleCalibTicks:
            self.throttleCalibData.append(self.gameModel.getCurrentCarSpeed()/60.0)

    def on_car_positions_const_thrott(self, data, extra):
        self.gameModel.updateGameTickInfo(data, extra)
        self.currentThrottle = 0.5
        self.throttle(self.currentThrottle)

    def on_crash(self, data, extra):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data, extra):
        print("Race ended")
        self.ping()

    def on_game_init(self, data, extra):
        print("Game init")
        #print(data)
        self.gameModel.initTrackData(data)
        self.gameModel.setOptimalLaneStrategy(game.gameDataModel.OPTIMAL_LANE_CURVATURE)

    def on_turbo(self, data, extra):
        print("Turbo available!")

    def on_error(self, data,extra):
        print("Error: {0}".format(data))
        self.ping()

    def lap_changed(self):
        print("Lap has changed for our car!")
        self.lastSwitchIndex = -1

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            #'carPositions': self.on_car_positions,
            #'carPositions':self.on_car_positions_time_est,
            'carPositions':self.on_car_positions_const_thrott,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'gameInit': self.on_game_init,
            'turboAvailable': self.on_turbo,
            'error': self.on_error,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            extraParam = None
            if msg_type == 'carPositions' and msg.has_key('gameTick'):
                extraParam = msg['gameTick']
            if msg_type in msg_map:
                msg_map[msg_type](data, extraParam)
            else:
                print("Got {0}".format(msg_type))
                print(data)
                self.ping()
            line = socket_file.readline()
        if self.botStopCallback != None:
            self.botStopCallback()
