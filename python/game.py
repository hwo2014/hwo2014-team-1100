import threading
import track
import math
import operator
import bgmath

import json
import socket
import threading

class BaseBot(threading.Thread):
    def __init__(self, socket, name, key):
        super(BaseBot, self).__init__()
        self.botRunning = threading.Semaphore(0)
        self.socket = socket
        self.name = name
        self.key = key
        self.gameModel = gameDataModel()
        self.gameModel.setOwnCarInfo(self.name, self.onLapChanged)
        self.botStopCallback = None
        self.trackName = 'keimola'
        self.DEBUG = False
        self.numberOfCars = 1
        self.gamePassword = None
        self.createOrJoin = True    # True -> Create a new game, False -> join
        self.simpleJoin = False

    def setTrack(self, track):
        self.trackName = track

    def setCreateJoin(self, switch):
        self.createOrJoin = switch

    def setNumberOfCars(self, carCount):
        self.numberOfCars = carCount

    def setGamePassword(self, password):
        self.gamePassword = password

    def setBotStopCallback(self, cb):
        self.botStopCallback = cb

    def getGameModel(self):
        return self.gameModel

    def msgToServer(self, msg_type, data, debugMsg = False):
        self.send(json.dumps({"msgType": msg_type, "data": data}), debugMsg)

    def send(self, msg, debugMsg = False):
        if debugMsg == True:
            print(msg)
        self.socket.send(msg + "\n")

    def createDataForRace(self):
        data = {}
        data["botId"] = { "name": self.name, "key": self.key}
        data["trackName"] = self.trackName
        if self.gamePassword != None:
            data["password"] = self.gamePassword
        data["carCount"] = int(self.numberOfCars)
        return data

    def joinARace(self):
        data = self.createDataForRace()
        #print("Joining race with : %s"%(format(data)))
        self.msgToServer("joinRace", data, True)

    def createARace(self):
        data = self.createDataForRace()
        #print("Creating a new race with : %s"%(format(data)))
        self.msgToServer("createRace", data, True)

    def setThrottle(self, throttle):
        self.msgToServer("throttle", throttle)

    def useSimpleJoin(self):
        self.simpleJoin = True

    def sendSimpleJoin(self):
        print("Using simple join!")
        self.msgToServer("join",{ "name": self.name, "key": self.key})

    def switchLane(self, right):
        if right:
            if self.DEBUG == True:
                print("Switching lane to right!")
            self.msgToServer("switchLane","Right")
        else:
            if self.DEBUG == True:
                print("Switching lane to left!")
            self.msgToServer("switchLane","Left")

    def useTurbo(self):
        self.msgToServer("turbo", "maximum powah!")

    def ping(self):
        self.msgToServer("ping", {})

    def waitUntilBotDone(self):
        self.botRunning.acquire()

    def run(self):
        if self.simpleJoin == True:
            self.sendSimpleJoin()
        else:
            if self.createOrJoin == True:
                self.createARace()
            else:
                self.joinARace()
        self.msgProcessingLoop()
        self.botRunning.release()

    def onJoin(self, data, extra):
        print("Joined %s"%(data))
        self.ping()

    def onJoinRace(self, data, extra):
        print("Joined race!")
        self.ping()

    def onYourCar(self, data, extra):
        print("Your car!")
        self.ping()

    def onGameStart(self, data, extra):
        print("Race started")
        self.ping()

    def onCarPositions(self, data, extra):
        canThrottle = True
        self.gameModel.updateGameTickInfo(data, extra)

    def onCrash(self, data, extra):
        print("Someone crashed")
        self.ping()

    def onGameEnd(self, data, extra):
        print("Race ended")
        self.ping()

    def onGameInit(self, data, extra):
        print("Game init")
        self.gameModel.initTrackData(data)

    def onTurbo(self, data, extra):
        print("Turbo available!")
        self.ping()

    def onError(self, data,extra):
        print("Error: {0}".format(data))
        self.ping()

    # this is useless since the server sends a lap change notification
    # when ever car changes a lap
    def onLapChanged(self):
        print("Lap has changed for our car!")

    def onLapFinished(self, data, extra):
        print("Server lap change")
        self.ping()

    def onFinish(self, data, extra):
        print("Finished!")
        self.ping()

    def onTournamentEnd(self, data, extra):
        print("Tournament ended!")
        self.ping()

    def onCreateRace(self, data, extra):
        print("Server race create!")
        self.ping()

    def msgProcessingLoop(self):
        msg_map = {
            'join': self.onJoin,
            'gameStart': self.onGameStart,
            'carPositions':self.onCarPositions,
            'crash': self.onCrash,
            'gameEnd': self.onGameEnd,
            'gameInit': self.onGameInit,
            'turboAvailable': self.onTurbo,
            'error': self.onError,
            'lapFinished': self.onLapFinished,
            'joinRace': self.onJoinRace,
            'yourCar': self.onYourCar,
            'finish': self.onFinish,
            'tournamentEnd': self.onTournamentEnd,
            'createRace': self.onCreateRace,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type != 'carPositions':
                print("got ->%s<-"%(msg_type))
                if msg_type != 'gameInit':
                    print("  with data : %s"%(format(data)))
            extraParam = None
            if msg_type == 'carPositions' and msg.has_key('gameTick'):
                extraParam = msg['gameTick']
            if msg_type in msg_map:
                msg_map[msg_type](data, extraParam)
            else:
                print("Unprocessed message -> {0}".format(msg_type))
                print(data)
                self.ping()
            line = socket_file.readline()
        if self.botStopCallback != None:
            self.botStopCallback()

class Car(object):
    def __init__(self, width, length, guide, name):
        self.width = width
        self.length = length
        self.guidePos = guide
        self.name = name
        self.prevPos = track.Point(0,0)
        self.curPos = track.Point(0,0)
        self.currentSpeed = 0.0

        self.currentTurnAngle = 0.0 # current heading calculated from received data
        self.inPieceDistance = 0.0  # distance travelled inside current piece
        self.pieceIndex = 0         # index of the piece the car is currently inside
        self.pieceLaneIdx = 0       # index of the lane car is currently on
        self.pieceLaneEndIdx = 0    # end index of the lane the car is going to if switching
        self.pieceLap = -1           # current lap the car is on
        self.pieceAngle = 0.0       # slippage angle received from server
        self.directionVector = None
        self.midPoint = None

    def isSwitching(self):
        return self.pieceLaneIdx != self.pieceLaneEndIdx

    # if dirvect is defined then angle is the deviation from dirVect
    def createCarPiece(self, midPoint, angle, dirVect = None):
        piece = track.TrackPiece(track.TrackPiece.TYPE_UNKNOWN)

        if dirVect != None:
            dirVector = track.Point.rotateVector(dirVect, angle)
            sideVector = track.Point.rotateVector(dirVect, angle+90.0)
        else:
            dirVector = track.Point(math.cos(track.ourAngleToMathAngleRads(angle)), math.sin(track.ourAngleToMathAngleRads(angle)))
            sideVector = track.Point(math.cos(track.ourAngleToMathAngleRads(angle+90.0)), math.sin(track.ourAngleToMathAngleRads(angle+90.0)))

        halfWidth = (self.width / 2.0)
        backHalf = (self.length - self.guidePos)
        guidePos = self.guidePos
        piece.addVertice((midPoint[0]+(dirVector.x*guidePos)+(sideVector.x*halfWidth), midPoint[1]+(dirVector.y*guidePos)+(sideVector.y*halfWidth)))
        piece.addVertice((midPoint[0]+(dirVector.x*guidePos)-(sideVector.x*halfWidth), midPoint[1]+(dirVector.y*guidePos)-(sideVector.y*halfWidth)))
        piece.addVertice((midPoint[0]-(dirVector.x*backHalf)-(sideVector.x*halfWidth), midPoint[1]-(dirVector.y*backHalf)-(sideVector.y*halfWidth)))
        piece.addVertice((midPoint[0]-(dirVector.x*backHalf)+(sideVector.x*halfWidth), midPoint[1]-(dirVector.y*backHalf)+(sideVector.y*halfWidth)))
        piece.setMiddlePoint((midPoint[0], midPoint[1]))

        return piece

class gameDataModel(object):
    OPTIMAL_LANE_CURVATURE = 1
    OPTIMAL_LANE_DISTANCE = 2
    def __init__(self):
        self.initLock = threading.Semaphore(0)
        self.tickCounter = -1        # current game tick (1 TICK == 16.666 ms)
        self.prevRecvTime = 0
        self.currentRecvTime = 0    # time in ms calculated from tick counter
        self.ownName = ""           # name of the car which is ours
        self.lapChangeCallback = None
        self.logFileGen = None
        self.logFileRecv = None

        self.throttleModelLag = 3
        self.throttleModelConstantAccel = (-5.17/256.0)
        self.throttleModelConstantDecel = (-5.17/256.0)
        self.throttleModelMul = 1.0

        self.slipConstant = 3000 #Used for cruise speed calculation - default is intentionally a bit high

    def setThrottleModel(self, throttleMul, constant):
        print("Throttle model set to M: %f C: %f"%(throttleMul, constant))
        self.throttleModelConstantAccel = constant
        self.throttleModelConstantDecel = constant
        self.throttleModelMul = throttleMul

    def setOwnCarInfo(self, name, lapcb):
        self.ownName = name
        self.lapChangeCallback = lapcb

    def checkForDataReady(self):
        self.initLock.acquire()

    def getTrackDimensions(self):
        return (self.trackDimens[0], self.trackDimens[1], self.trackScale)

    def getTickCounter(self):
        return self.tickCounter

    def getCarForName(self, name):
        for car in self.carData:
            if car.name == name:
                return car
        return None

    def nextSwitchPieces(self):
        car = self.getCarForName(self.ownName)
        if car != None:
            for idx in self.switchIndices:
                if idx >= car.pieceIndex:
                    return idx
            # close to the end, so return first
            return self.switchIndices[0]
        else:
            print("Couldn't find self for next switch lane check!")

    def laneStillOptimalAfterIndex(self, pieceIndex):
        car = self.getCarForName(self.ownName)
        if car != None:
            return car.pieceLaneIdx == self.optLanes[pieceIndex+1]
        else:
            print("Couldn't find self for future optimal lane check!")

    def getCurrentPieceIndex(self):
        car = self.getCarForName(self.ownName)
        if car != None:
            return car.pieceIndex
        else:
            print("Couldn't find self for piece index!")

    def isOnSwitchPiece(self):
        car = self.getCarForName(self.ownName)
        if car != None:
            return self.originalTrack[car.pieceIndex].switch
        else:
            print("Couldn't find self for switch lane check!")

    def isSwitching(self):
        car = self.getCarForName(self.ownName)
        if car != None:
            return car.isSwitching()
        else:
            print("Couldn't find self for switching in progress check!")

    def isOnOptimalLane(self):
        car = self.getCarForName(self.ownName)
        if car != None:
            return car.pieceLaneIdx == self.optLanes[car.pieceIndex]
        else:
            print("Couldn't find self for optimal lane check!")

    def getCurrentCarSpeed(self):
        car = self.getCarForName(self.ownName)
        if car != None:
            return car.currentSpeed
        else:
            print("Couldn't find self for speed check!")

    def switchToRight(self, preSwitchIndex):
        car = self.getCarForName(self.ownName)
        if car != None:
            currentOptLane = self.optLanes[preSwitchIndex]
            futureOptLane = self.optLanes[preSwitchIndex+2]
            if currentOptLane == futureOptLane:
                print("Same lane, %d vs. %d"%( self.laneInfo[car.pieceLaneIdx].distance , self.laneInfo[futureOptLane].distance))
                return self.laneInfo[car.pieceLaneIdx].distance < self.laneInfo[futureOptLane].distance
            else:
                print("Different lane, %d vs. %d"%(self.laneInfo[currentOptLane].distance , self.laneInfo[futureOptLane].distance))
                return self.laneInfo[currentOptLane].distance < self.laneInfo[futureOptLane].distance
        else:
            print("Couldn't find self lane switch direction change!")

    def getTrackPiece(self, index):
        pieceCount = len(self.originalTrack)
        # we need to support negative indices
        return self.originalTrack[index%pieceCount]

    def getTrackData(self):
        return self.originalTrack

    def getLaneInfo(self):
        return self.laneInfo

    def getLaneCount(self):
        return len(self.laneInfo)

    def setOptimalLaneStrategy(self, selection):
        if selection == self.OPTIMAL_LANE_CURVATURE:
            self.optLanes = self.optimalLanesCurvature
        elif selection == self.OPTIMAL_LANE_DISTANCE:
            self.optLanes = self.optimalLanesDistance
        else:
            print("Attempting to set unknown : %d type for optimal lane strategy!"%(selection))

    def getOptimalLanes(self):
        return self.optLanes

    def getTrackWidth(self):
        return self.trackWidth

    def getCarData(self):
        return self.carData

    def setLogFileGen(self, fileName):
        self.logFileGen = open(fileName,"wt")

    def closeLogGen(self):
        self.logFileGen.close()

    def setLogFileRecv(self, fileName):
        print("Saving data to file %s"%(fileName))
        self.logFileRecv = open(fileName,"wt")

    def closeLogRecv(self):
        self.logFileRecv.close()


    # handles data from gameInit JSON message
    def initTrackData(self, gameInitData):
        if self.logFileRecv != None:
            self.logFileRecv.write(format(gameInitData)+"\n")

        creator = track.TrackCreator((0.0,0.0),180.0)
        trackData = creator.generateTrackData(gameInitData['race']['track'])
        cars = gameInitData['race']['cars']
        self.carData = []
        self.trackWidth = creator.currentTrackWidth
        for car in cars:
            newCar = Car(car['dimensions']['width'], car['dimensions']['length'],car['dimensions']['guideFlagPosition'],car['id']['name'])
            self.carData.append(newCar)
        self.originalTrack = trackData[0]
        self.laneInfo = trackData[1]
        self.trackLaneIndices = trackData[2]

        self.calculateTrackDimensions()
        self.calculateSwitchIndicesAndSearchRanges()
        self.calculateOptimalByCurvature()
        self.calculateOptimalLaneLowestDistance()
        self.calculateTrackMeters() #SAKU

        print("Got track data and initialized!")
        self.initLock.release()

    # handles data from carPositions JSON message
    def updateGameTickInfo(self, tick, tickValue):
        if tickValue != None:
            self.setGameTick(tickValue)
        timeDiff = (self.currentRecvTime-self.prevRecvTime)
        if self.logFileRecv != None and tickValue != None:
            line = "{u'data':"+format(tick)+", u'gameTick': %d}"%(tickValue)
            self.logFileRecv.write(line+"\n")
        for car in tick:
            pieceName = car['id']['name']
            carObject = self.getCarForName(pieceName)
            if carObject != None:
                carObject.inPieceDistance = float(car['piecePosition']['inPieceDistance'])
                carObject.pieceIndex = car['piecePosition']['pieceIndex']
                carObject.pieceLaneIdx = car['piecePosition']['lane']['startLaneIndex']
                carObject.pieceLaneEndIdx = car['piecePosition']['lane']['endLaneIndex']

                newLap = car['piecePosition']['lap']
                if newLap > carObject.pieceLap and carObject.name == self.ownName and self.lapChangeCallback != None:
                    self.lapChangeCallback()
                carObject.pieceLap = newLap
                carObject.pieceAngle = float(car['angle'])

                carObject.prevPos.x = carObject.curPos.x
                carObject.prevPos.y = carObject.curPos.y
                piece = self.originalTrack[carObject.pieceIndex]
                if carObject.isSwitching():   # if we're switching we're on a bezier curve
                    currentLane = piece.lanes[carObject.pieceLaneIdx]
                    newLane =  piece.lanes[carObject.pieceLaneEndIdx]

                    t = carObject.inPieceDistance / currentLane.getLaneSwitchLength(newLane.index)
                    middlePoint1 = track.Point((currentLane.startPoint.x+currentLane.endPoint.x)/2,(currentLane.startPoint.y+currentLane.endPoint.y)/2)
                    middlePoint2 = track.Point((newLane.startPoint.x+newLane.endPoint.x)/2,(newLane.startPoint.y+newLane.endPoint.y)/2)
                    carObject.directionVector = bgmath.getQuadraticBezierPointDerivate(track.Point(currentLane.startPoint.x, currentLane.startPoint.y),
                                                     track.Point(middlePoint1.x, middlePoint1.y),
                                                     track.Point(middlePoint2.x, middlePoint2.y),
                                                     track.Point(newLane.endPoint.x, newLane.endPoint.y),t)
                    carObject.curPos = bgmath.getQuadraticBezierPoint(track.Point(currentLane.startPoint.x, currentLane.startPoint.y),
                                                     track.Point(middlePoint1.x, middlePoint1.y),
                                                     track.Point(middlePoint2.x, middlePoint2.y),
                                                     track.Point(newLane.endPoint.x, newLane.endPoint.y),t)
                else:                   # if we're not switching we'll do more accurate location
                    carObject.directionVector = None
                    if piece.type == track.TrackPiece.TYPE_STRAIGHT:
                        dirVect = track.Point.createUnit(piece.realAngle)
                        laneStartPoint = piece.getLaneByIndex(carObject.pieceLaneIdx).startPoint
                        dx = dirVect.x*carObject.inPieceDistance
                        dy = dirVect.y*carObject.inPieceDistance
                        carObject.currentTurnAngle = piece.pieceDirection
                        carObject.curPos = track.Point(laneStartPoint.x+dx, laneStartPoint.y+dy)
                    elif piece.type == track.TrackPiece.TYPE_CURVED:
                        lane = piece.getLaneByIndex(carObject.pieceLaneIdx)
                        carObject.curPos, carObject.currentTurnAngle = self.calculatePointOnArc(piece.arcMidPoint, piece.originalAngle,
                                        piece.turnRadius, lane.distance, carObject.inPieceDistance, piece.pieceDirection)
                        carObject.currentTurnAngle += piece.pieceDirection

                if carObject.directionVector != None:
                    dirVectMidPoint = track.Point.rotateVector(carObject.directionVector, carObject.pieceAngle)
                else:
                    dirVectMidPoint = track.Point.createUnit(carObject.currentTurnAngle+carObject.pieceAngle)
                dist = (carObject.length/2) - carObject.guidePos
                carMidPoint = track.Point(carObject.curPos.x-dirVectMidPoint.x*dist, carObject.curPos.y-dirVectMidPoint.y*dist)
                if carObject.midPoint != None:
                    midPointDiff = track.Point(carMidPoint.x-carObject.midPoint.x, carMidPoint.y-carObject.midPoint.y)
                else:
                    midPointDiff = track.Point(0,0)
                midPointDiffTravel = track.Point.distanceBetween(track.Point(0,0), midPointDiff)
                carObject.midPoint = carMidPoint
                carSpeedVector = track.Point(carObject.curPos.x-carObject.prevPos.x, carObject.curPos.y-carObject.prevPos.y)

                distTrav = track.Point.distanceBetween(carObject.curPos, carObject.prevPos)
                if self.logFileGen != None and carObject.name == self.ownName:
                    currentLane = piece.lanes[carObject.pieceLaneIdx]
                    curv = self.curvature(carObject.pieceIndex, carObject.pieceLaneIdx, carObject.inPieceDistance, 1)
                    outLine = "%s tick:\t%d\tlocx:\t%.3f\tlocy:\t%.3f\tangle:\t%.3f\tcurv:\t%.3f\tdistTrav:\t%.6f\tsloc:\t%f\t%f\tloc:\t%f\t%f\tlen:\t%f\trad:\t%f\n"%(carObject.name,
                            self.tickCounter,carObject.curPos.x,carObject.curPos.y,carObject.pieceAngle,curv,distTrav,carSpeedVector.x,carSpeedVector.y,midPointDiff.x,midPointDiff.y,midPointDiffTravel,currentLane.laneRadius)
                    self.logFileGen.write(outLine)
                if timeDiff != 0:
                    carObject.currentSpeed = (distTrav*1000.0/timeDiff)
            else:
                print("Received data for an unknown car : %s"%(pieceName))

    def calculatePointOnArc(self, arcMidPoint, arcAngle, radiusMiddle, radiusDiff, distanceOnArc, arcDirectionAngle):
        if arcAngle > 0:
            actualRadius = radiusMiddle - radiusDiff
        else:
            actualRadius = radiusMiddle + radiusDiff
        arcLength = track.TrackCreator.calculateArcLength(radiusMiddle, arcAngle, radiusDiff)
        turnAngle = (distanceOnArc/arcLength)*arcAngle
        if arcAngle > 0:
            finalAngle = arcDirectionAngle + turnAngle - 90
        else:
            finalAngle = arcDirectionAngle + turnAngle + 90
        dirVect = track.Point.createUnit(finalAngle)
        return (track.Point(arcMidPoint.x+dirVect.x*actualRadius, arcMidPoint.y+dirVect.y*actualRadius), turnAngle)

    def findPieceForPos(self, startindex, laneIdx, pos, posdiff):
        piece = self.getTrackPiece(startindex)
        laneData = piece.getLaneByIndex(laneIdx)
        newpos = (pos+posdiff)
        if  newpos >= 0.0 and newpos < laneData.length:
            return (piece, newpos)
        elif newpos < 0.0:
            idx = -1
            while newpos < 0:
                piece = self.getTrackPiece(startindex+idx)
                newpos += piece.getLaneByIndex(laneIdx).length
                idx -= 1
            return (piece, newpos)
        else:
            idx = 1
            while newpos > laneData.length:
                newpos -= laneData.length
                piece = self.getTrackPiece(startindex+idx)
                laneData = piece.getLaneByIndex(laneIdx)
                idx += 1
            return (piece, newpos)

    #TODO add support for lane switch
    def calculatePosInPiece(self, piece, laneIdx, distance):
        if piece.type == track.TrackPiece.TYPE_STRAIGHT:
            dirVect = track.Point.createUnit(piece.realAngle)
            laneStartPoint = piece.getLaneByIndex(laneIdx).startPoint
            dx = dirVect.x*distance
            dy = dirVect.y*distance
            return (laneStartPoint.x+dx, laneStartPoint.y+dy)
        elif piece.type == track.TrackPiece.TYPE_CURVED:
            lane = piece.getLaneByIndex(laneIdx)
            newPos, turnAngle = self.calculatePointOnArc(piece.arcMidPoint, piece.originalAngle,
                            piece.turnRadius, lane.distance, distance, piece.pieceDirection)
            return (newPos.x, newPos.y)

    def deriv1ord4acc(self, x2m, x1m, x, x1p, x2p, h):
        return (x2m-(x1m*8)+(x1p*8)-x2p)/(12*h)

    def deriv2ord4acc(self, x2m, x1m, x, x1p, x2p, h):
        return (-x2m+(x1m*16)-(x*30)+(x1p*16)-x2p)/(12*h*h)

    def curvature(self, index, lane, pos, h):
        piece = self.getTrackPiece(index)
        piece1m, pos1m = self.findPieceForPos(index, lane, pos, -h)
        piece2m, pos2m = self.findPieceForPos(index, lane, pos, -(2*h))
        piece1p, pos1p = self.findPieceForPos(index, lane, pos, h)
        piece2p, pos2p = self.findPieceForPos(index, lane, pos, 2*h)
        t = self.calculatePosInPiece(piece, lane, pos)
        t1m = self.calculatePosInPiece(piece1m, lane, pos1m)
        t2m = self.calculatePosInPiece(piece2m, lane, pos2m)
        t1p = self.calculatePosInPiece(piece1p, lane, pos1p)
        t2p = self.calculatePosInPiece(piece2p, lane, pos2p)
        x1st = self.deriv1ord4acc(t2m[0], t1m[0], t[0], t1p[0], t2p[0], h)
        y1st = self.deriv1ord4acc(t2m[1], t1m[1], t[1], t1p[1], t2p[1], h)
        x2nd = self.deriv2ord4acc(t2m[0], t1m[0], t[0], t1p[0], t2p[0], h)
        y2nd = self.deriv2ord4acc(t2m[1], t1m[1], t[1], t1p[1], t2p[1], h)
        m = math.sqrt(x1st*x1st+y1st*y1st)
        c = (x1st*y2nd-y1st*x2nd)/(m*m*m)
        return c

    def calculateSwitchIndicesAndSearchRanges(self):
        maxPieceIndex = len(self.originalTrack)
        self.switchIndices = []
        for pieceIdx in range(maxPieceIndex):
            piece = self.originalTrack[pieceIdx]
            if piece.switch == True:
                self.switchIndices.append(pieceIdx)
        maxIdx = len(self.switchIndices)
        # create index search ranges from between switch blocks
        self.searchRanges = []
        for idx in range(maxIdx-1):
            self.searchRanges.append(range(self.switchIndices[idx]+1, self.switchIndices[idx+1]))
        lastRange = range(self.switchIndices[maxIdx-1]+1, maxPieceIndex)+range(0, self.switchIndices[0])
        if len(lastRange) > 0:
            self.searchRanges.append(lastRange)

    def calculateOptimalByCurvature(self):
        optimalLanes = {}
        laneCount = len(self.laneInfo)
        # first calculate curvature for each block by averaging
        # the curvature in 10 positions on each lane and then
        # choose the lane with the lowest absolute curvature
        # as optimal for each piece
        for pieceIdx in range(len(self.originalTrack)):
            piece = self.originalTrack[pieceIdx]
            laneInfo = {}
            for laneIdx in range(laneCount):
                lane = piece.getLaneByIndex(laneIdx)
                step = lane.length / 10.0
                stepLoc = 0.0
                total = 0
                curvTot = 0.0
                while stepLoc < lane.length:
                    curv = self.curvature(pieceIdx, laneIdx, stepLoc, 2)
                    stepLoc += step
                    total += 1
                    curvTot += curv
                avgCurv = curvTot / total
                laneInfo[laneIdx] = abs(avgCurv)
            optimLane = min(laneInfo.iteritems(), key=operator.itemgetter(1))[0]
            if piece.type == track.TrackPiece.TYPE_STRAIGHT:
                optimalLanes[pieceIdx] = -1
            else:
                optimalLanes[pieceIdx]=optimLane
        # next choose a lane with most optimum selections by curvature
        # between two switch points as the optimal lane
        # NOTE: straight pieces are ignored here since their curvature
        # is mostly zero, it only changes a little at the end if there
        # is a tight curve at the end. this may have an effect on
        # complicated tracks with tight curves following straight lanes
        # create index search ranges from between switch blocks

        # count max optimal selections for each search range
        maxIdx = len(self.searchRanges)
        finalOptimalLane = [0]*maxIdx
        for searchIdx in range(len(self.searchRanges)):
            search = self.searchRanges[searchIdx]
            optimalLaneHistogram = [0] * laneCount
            for sIdx in search:
                if optimalLanes[sIdx] != -1:
                    optimalLaneHistogram[optimalLanes[sIdx]] += 1
            maxCount = max(optimalLaneHistogram)
            maxCountIdx = optimalLaneHistogram.index(maxCount)
            finalOptimalLane[searchIdx] = maxCountIdx
        for searchIdx in range(len(self.searchRanges)):
            search = self.searchRanges[searchIdx]
            for sIdx in search:
                optimalLanes[sIdx] = finalOptimalLane[searchIdx]
        self.optimalLanesCurvature = optimalLanes

    def calculateOptimalLaneLowestDistance(self):
        optimalLanes = {}
        switchIndices = []
        indices = range(len(self.originalTrack))
        # first pass choose optimal lanes for curves (inside lane)
        for pieceIdx in indices:
            piece = self.originalTrack[pieceIdx]
            if piece.type == track.TrackPiece.TYPE_CURVED:
                if piece.originalAngle > 0: #right hand turn
                    optimalLanes[pieceIdx] = self.trackLaneIndices[1]
                else:                       #left hand turn
                    optimalLanes[pieceIdx] = self.trackLaneIndices[0]
            if piece.switch == True:
                switchIndices.append(pieceIdx)
        # second pass choose optimal lanes for straight (inside lane in next curve)
        for pieceIdx in indices:
            piece = self.originalTrack[pieceIdx]
            if piece.type == track.TrackPiece.TYPE_STRAIGHT:
                searchRange = indices[pieceIdx+1:len(indices)]+indices[0:pieceIdx]
                for curveIdx in searchRange:
                    if self.originalTrack[curveIdx].type == track.TrackPiece.TYPE_CURVED:
                        optimalLanes[pieceIdx] = optimalLanes[curveIdx]
                        break
        # account for switches, optimal may not be optimal if you can't switch to
        # it so choose the best possible -> lane with highest total optimal count
        # until next switch

        # count max optimal selections for each search range
        maxIdx = len(self.searchRanges)
        finalOptimalLane = [0]*maxIdx
        for searchIdx in range(len(self.searchRanges)):
            search = self.searchRanges[searchIdx]
            optimalLaneHistogram = [0] * (1+(self.trackLaneIndices[1]-self.trackLaneIndices[0]))
            for sIdx in search:
                optimalLaneHistogram[optimalLanes[sIdx]] += 1
            maxCount = max(optimalLaneHistogram)
            maxCountIdx = optimalLaneHistogram.index(maxCount)
            finalOptimalLane[searchIdx] = maxCountIdx
        for searchIdx in range(len(self.searchRanges)):
            search = self.searchRanges[searchIdx]
            for sIdx in search:
                optimalLanes[sIdx] = finalOptimalLane[searchIdx]
        self.optimalLanesDistance = optimalLanes

    # for the extra tick field in carPositions JSON message
    def setGameTick(self, tick):
        self.tickCounter = tick
        self.prevRecvTime = self.currentRecvTime
        self.currentRecvTime = self.tickCounter * (100.0/6.0)

    def calculateTrackDimensions(self):
        minX = minY =  1000000
        maxX = maxY = -1000000
        for piece in self.originalTrack:
            for coord in piece.vertices:
                if coord[0] < minX:
                    minX = coord[0]
                if coord[0] > maxX:
                    maxX = coord[0]
                if coord[1] < minY:
                    minY = coord[1]
                if coord[1] > maxY:
                    maxY = coord[1]
        self.trackDimens = (track.Point(minX, minY), track.Point(maxX, maxY))
        self.trackScale = track.Point(self.trackDimens[1].x-self.trackDimens[0].x, self.trackDimens[1].y-self.trackDimens[0].y)

    # calculates the speed at some future tick
    def speedAtTick(self, targetTick, throttle):
        currentSpeed = self.getCurrentCarSpeed()/60.0
        currentTick = self.getTickCounter()
        targetSpeed = throttle*self.throttleModelMul
        return (currentSpeed + (targetSpeed - currentSpeed)*(1.0-math.exp(self.throttleModelConstantAccel*(targetTick-currentTick))))

    # calculates how many ticks are required to reach certain speed
    def ticksRequiredToReachSpeed(self, targetSpeed, throttle):
        currentSpeed = self.getCurrentCarSpeed()/60.0
        currentTick = self.getTickCounter()
        throttleAdj = throttle*self.throttleModelMul
        targetU = throttleAdj-targetSpeed
        targetL = throttleAdj-currentSpeed
        if targetU == 0.0:
            targetU = throttleAdj-(targetSpeed-0.0001)
        if targetL == 0.0:
            targetL = throttleAdj-(origSpeed-0.0001)
        return int(currentTick+math.log(targetU/targetL)/self.throttleModelConstantAccel)

    def speedAtTickAccel(self, tickToCalc, origSpeed, throttle, origTick):
        return (origSpeed + (10*throttle-origSpeed)*(1.0-math.exp(self.throttleModelConstantAccel*(tickToCalc-origTick))))

    def speedAtTickDecel(self, tickToCalc, origSpeed, throttle, origTick):
        return (10*throttle + (origSpeed-10*throttle)*math.exp(self.throttleModelConstantDecel*(tickToCalc-origTick)))

    def ticksToReachVelAccel(self, origTick, targetSpeed, origSpeed, throttle):
        throttleAdj = 10*throttle
        targetU = throttleAdj-targetSpeed
        targetL = throttleAdj-origSpeed
        if targetU == 0.0:
            targetU = throttleAdj-(targetSpeed-0.0001)
        if targetL == 0.0:
            targetL = throttleAdj-(origSpeed-0.0001)
        return int(origTick+self.throttleModelLag+math.log(targetU/targetL)/self.throttleModelConstantAccel)

    def ticksToReachVelDecel(self, origTick, targetSpeed, origSpeed, throttle):
        throttleAdj = 10*throttle
        targetU = targetSpeed-throttleAdj
        targetL = origSpeed-throttleAdj
        if targetU == 0.0:
            targetU = targetSpeed-(throttleAdj-0.0001)
        if targetL == 0.0:
            targetL = origSpeed-(throttleAdj-0.0001)
        return int(origTick+self.throttleModelLag+math.log(targetU/targetL)/self.throttleModelConstantDecel)

    @staticmethod
    def meanSquareError(ary1, ary2):
        totalSE = 0.0
        cmpLen = len(ary1)
        if len(ary2) < cmpLen:
            cmpLen = len(ary2)
        for idx in range(cmpLen):
            d = ary1[idx]-ary2[idx]
            diff = math.sqrt(d*d)
            totalSE += diff
            #print("%f\t%f\t%f"%(ary1[idx],ary2[idx],diff))
        MSE = (totalSE / cmpLen)
        #print(MSE)
        return MSE

    def calculateAccelModelConstants(self, tickToReachVel, velocity, throttle, data, lag):
        initialConstant = -math.log(1.0-(velocity/(10.0*throttle)))
        self.throttleModelLag = lag-1
        self.throttleModelConstantAccel = -initialConstant/tickToReachVel

        cmpAry = []
        for idx in range(1,tickToReachVel):
            value = self.speedAtTickAccel(idx, 0, throttle, 0)
            if value < 0.0:
                value = 0.0
            cmpAry.append(value)

        prevMSE = gameDataModel.meanSquareError(data,cmpAry)

        prevChange = 0.5
        self.throttleModelConstantAccel = -initialConstant/tickToReachVel

        ok = True
        failCounter = 0
        while ok and failCounter < 50:
            cmpAry = []
            for idx in range(1,tickToReachVel):
                value = self.speedAtTickAccel(idx, 0, throttle, 0)
                if value < 0.0:
                    value = 0.0
                cmpAry.append(value)

            MSE = gameDataModel.meanSquareError(data,cmpAry)
            MSEratio = MSE/prevMSE

            if MSE > prevMSE: # error growing
                prevChange = -1.0*prevChange
                prevChange *= 0.90
            else:
                prevChange *= 0.70
            initialConstant += prevChange

            self.throttleModelConstantAccel = -initialConstant/tickToReachVel

            if MSE < 0.001:
                ok = False
            prevMSE = MSE
            failCounter = failCounter+1
        self.throttleModelConstantDecel = self.throttleModelConstantAccel
        print("Final constant accel: %f (%f), took %d iterations"%(self.throttleModelConstantAccel,initialConstant,failCounter))

    def calculateDecelModelConstants(self, tickToBegin, tickToReachVel, velocityBegin, velocityTarget, throttle, data):
        print("calculateDecelModelConstants(%f,%f,%f,%f,%f)"%(tickToBegin, tickToReachVel, velocityBegin, velocityTarget, throttle))
        initialConstant = -math.log((velocityTarget/velocityBegin))
        #print(initialConstant)
        self.throttleModelConstantDecel = -initialConstant/(tickToReachVel-tickToBegin)

        cmpAry = []
        for idx in range(tickToBegin+1, tickToReachVel):
            value = self.speedAtTickDecel(idx, velocityBegin, throttle, tickToBegin)
            if value < 0.0:
                value = 0.0
            cmpAry.append(value)

        prevMSE = gameDataModel.meanSquareError(data,cmpAry)

        prevChange = 0.5*prevMSE
        self.throttleModelConstantDecel = -initialConstant/(tickToReachVel-tickToBegin)

        ok = True
        failCounter = 0
        while ok and failCounter < 2:
            cmpAry = []
            for idx in range(1,tickToReachVel):
                value = self.speedAtTickDecel(idx, velocityBegin, throttle, tickToBegin)
                if value < 0.0:
                    value = 0.0
                cmpAry.append(value)

            MSE = gameDataModel.meanSquareError(data,cmpAry)
            MSEratio = MSE/prevMSE
            print("%f %f %f %f %f"%(MSE,self.throttleModelConstantDecel,prevMSE,MSEratio,prevChange))

            if MSE > prevMSE: # error growing
                prevChange = -1.0*prevChange
                prevChange *= 1.10
            else:
                prevChange *= 0.70
            initialConstant += prevChange

            self.throttleModelConstantDecel = -initialConstant/(tickToReachVel-tickToBegin)

            if MSE < 0.001:
                ok = False
            prevMSE = MSE
            failCounter = failCounter+1
        print("Final constant decel: %f (%f), took %d iterations"%(self.throttleModelConstantDecel,initialConstant,failCounter))

    #Calculate which is the max speed for curvature that does not inclrease car angle
    def calculateMaxSpeed(self, curv):
        if (curv == 0):
            return 2000 #Turbo might increase max speed over 1000
        maxSpeed = (self.slipConstant * 1/abs(curv))**(0.5)
        #print "CalculateMaxSpeed ms:",maxSpeed," curv:", curv
        return maxSpeed

    #Calculate optimal throttle to cruise in curve. It obviously must be bigger than throttle max speed
    def calculateCruiseThrottle(self, curv):
        maxSpeed = self.calculateMaxSpeed(curv)
        throttle = (maxSpeed/1000) * 1.5
        return throttle

    #Named "track ticks" as meters
    def calculateTrackMeters(self): #SAKU
        self.trackMeterCurv = []
        self.trackMeterMagic = []
        for laneIdx in range(0, self.getLaneCount()):
            cumulMagic = 0
            self.trackMeterMagic.append([])
            self.trackMeterCurv.append([])
            for pieceIdx in range(0, len(self.originalTrack)):
                pieceLen = self.originalTrack[pieceIdx].getLaneByIndex(laneIdx).length
                for piecePos in range(0,int(pieceLen)):
                    if (self.originalTrack[pieceIdx].turnRadius == 0.0):
                        curv = 0
                        if (abs(cumulMagic)<0.05):
                            cumulMagic = 0
                        elif (cumulMagic>0):
                            cumulMagic -= 0.05
                        elif (cumulMagic<0):
                            cumulMagic += 0.05
                    else:
                        curv = self.curvature(pieceIdx,laneIdx,piecePos, 2)
                        cumulMagic += curv
                        if (cumulMagic>2.0):
                            cumulMagic = 2.0
                        elif (cumulMagic<-2.0):
                            cumulMagic = -2.0
                    self.trackMeterCurv[laneIdx].append(curv)
                    self.trackMeterMagic[laneIdx].append(cumulMagic)
                    #print "l:",laneIdx," p:",pieceIdx," curv:", curv," magic:", cumulMagic

    def getMeterCurvature(self, laneIdx, pos):
        position = pos % len(self.trackMeterCurv[laneIdx])
        return self.trackMeterCurv[laneIdx][position]

    def getMeterMagic(self, laneIdx, pos):
        position = pos % len(self.trackMeterCurv[laneIdx])
        return self.trackMeterMagic[laneIdx][position]

    def calculateMeterPosition(self, pieceIdx, laneIdx, inPieceDist):
        pos = 0
        for pIdx in range(0, pieceIdx):
            pos += self.originalTrack[pIdx].getLaneByIndex(laneIdx).length
        pos += inPieceDist
        return pos

    def getMeterCount(self, laneIdx):
        return len(self.trackMeterCurv[laneIdx])

class speedCalibrator(threading.Thread):
    def __init__(self, accel, data, callback):
        super(speedCalibrator, self).__init__()
        self.calibForAccel = accel
        self.resultsCallback = callback
        self.dataAry = data

    def genExpForAccel(self, mult, const, max):
        genAry = []
        for idx in range(max):
            genAry.append(mult*(1.0-math.exp(idx*const)))
        return genAry

    #def genExpForDecel(mult, const, max):
    #    genAry = []
    #    for idx in range(max):
    #        genAry.append(mult*(math.exp(idx*const)))
    #    return genAry

    def meanSquareError(self, ary1, ary2):
        totalSE = 0.0
        cmpLen = len(ary1)
        if len(ary2) < cmpLen:
            cmpLen = len(ary2)
        for idx in range(cmpLen):
            d = ary1[idx]-ary2[idx]
            diff = math.sqrt(d*d)
            totalSE += diff
        MSE = (totalSE / cmpLen)
        return MSE

    def findMuls(self, mulMin, mulMax, cnsMin, cnsMax,gridSize):
        dataAmount = len(self.dataAry)
        MSEAry = []
        cnsDiff = cnsMax - cnsMin
        mulDiff = mulMax - mulMin
        gridMul = 1.0 / float(gridSize-1)
        for mulIdx in range(gridSize):
            MSErow = []
            mulVal = mulMin+((mulIdx * gridMul)*mulDiff)
            for cnsIdx in range(gridSize):
                genAry = self.genExpForAccel(mulVal,cnsMin+((cnsIdx * gridMul)*cnsDiff),dataAmount)
                MSErow.append(self.meanSquareError(self.dataAry, genAry))
            MSEAry.append(MSErow)
        return MSEAry

    def conv(self, x, xmi, xma, r):
        return xmi+((x/r)*(xma-xmi))

    def minForRange(self, mulMin, mulMax, cnsMin, cnsMax, gridSize):
        MSEres = self.findMuls(mulMin, mulMax, cnsMin, cnsMax, 10)

        minMSE = 16777216.0
        minRI = -1
        minCI = -1
        for rowIdx in range(len(MSEres)):
            row = MSEres[rowIdx]
            for valIdx in range(len(row)):
                val = row[valIdx]
                if val < minMSE:
                    minMSE = val
                    minRI = rowIdx
                    minCI = valIdx
        newCnsMin = self.conv(minCI-1, cnsMin, cnsMax, 9.0)
        newCnsMax = self.conv(minCI+1, cnsMin, cnsMax, 9.0)
        newMulMin = self.conv(minRI-1, mulMin, mulMax, 9.0)
        newMulMax = self.conv(minRI+1, mulMin, mulMax, 9.0)

        #print("C %f %f %f"%(newCnsMin, newCnsMax, newCnsMax-newCnsMin))
        #print("M %f %f %f"%(newMulMin, newMulMax, newMulMax-newMulMin))

        #print("%d,%d %f %f"%(minRI, minCI, conv(minRI, 1.0, 10.0, 9.0), conv(minCI, -0.01, -0.03, 9.0)))
        return (newCnsMin, newCnsMax, newMulMin, newMulMax)

    def run(self):
        a = (-0.001, -0.1, 1.0, 10.0)

        for i in range(10):
            a = self.minForRange(a[2],a[3], a[0],a[1], 10)

        self.resultsCallback((a[0]+a[1])/2, (a[2]+a[3])/2, self.calibForAccel)

