import game

class KekeBot(game.BaseBot):
    def __init__(self, socket, name, key):
        game.BaseBot.__init__(self, socket, name, key)
        # do personal initializations
        self.prevAngle = 0.0
        self.cruiseCalibCount = 1
        self.magicValueLimit = 0.5

        #constants
        self.CONST_SLIPANGLE = 60.0
        self.CONST_HIGHANGLE = 50.0
        self.CONST_LOWANGLE = 15.0

        #Lane switching
        self.haveThrottled = False
        self.needToSwitch = False
        self.lastSwitchIndex = -1
        self.nextIndexToSwitch = -1

        # do personal initializations
        self.accelList = [0.456] # [0.25, 0.45] #[0.15, 0.25, 0.35, 0.45]
        self.accelResCns = []
        self.accelResMul = []
        self.accelIdx = 0
        self.accelDataCount = -1
        self.accelSampleCount = 250 # 250 ticks seems to be roughly when speed maxs out
        self.accelData = []
        self.calibrator = None
        self.accelerating = True
        self.accelCalibDone = False
        self.prevSpeed = 0.0
        self.startFinalAccel = False
        self.calibResFinished = False



    def onGameInit(self, data, extra):
        game.BaseBot.onGameInit(self, data, extra)
        self.gameModel.setOptimalLaneStrategy(game.gameDataModel.OPTIMAL_LANE_CURVATURE)

    def speedCalibAccel(self):
        carSpeed = self.gameModel.getCurrentCarSpeed() / 60.0
        if self.accelerating == True:
            currentThrottle = self.accelList[self.accelIdx]
            if carSpeed > 0.0:  # if we're moving start recording
                if self.accelDataCount < 0: #start
                    print("Started recording accel data for throttle: %f"%(currentThrottle))
                    self.accelDataCount = 2
                    self.accelData = []
                    self.accelData.append(0.0)
                    self.accelData.append(carSpeed)
                elif self.accelDataCount < self.accelSampleCount:
                    self.accelData.append(carSpeed)
                    self.accelDataCount += 1
                else:   # finished
                    print("Finished with accel data for throttle: %f"%(currentThrottle))
                    self.calibrator = game.speedCalibrator(True, self.accelData, self.calibResults)
                    self.calibrator.start()
                    self.accelDataCount = -1
                    self.accelerating = False
                    self.accelIdx += 1
                    if self.accelIdx >= len(self.accelList):
                        self.accelCalibDone = True
        else:
            currentThrottle = 0.0
            if abs(self.prevSpeed-carSpeed) < 0.001 and carSpeed < 0.01:
                print("stopped decelaration at speed: %f %f"%(carSpeed, self.prevSpeed))
                self.accelerating = True
            self.prevSpeed = carSpeed

        self.cruiseCalibrate()
        self.prevAngle = self.gameModel.getCarForName(self.gameModel.ownName).pieceAngle

        return currentThrottle

    def onCarPositions(self, data, extra):
        game.BaseBot.onCarPositions(self, data, extra)
        currentThrottle = 1.0
        if (self.accelCalibDone == False):
            #print "Calibrating"
            currentThrottle = self.speedCalibAccel()
            self.setThrottle(currentThrottle)
        else:
            if self.startFinalAccel == False and self.calibResFinished == True:
                self.startFinalAccel = True
                ticks = self.gameModel.ticksRequiredToReachSpeed(0.98*1.0*self.gameModel.throttleModelMul, 1.0)
                speed = self.gameModel.speedAtTick(self.gameModel.getTickCounter()+50, 1.0)
                print("Estimating that it takes until %d ticks to reach: %f speed"%(ticks,0.98*1.0*self.gameModel.throttleModelMul*60.0))
                print("Estimating that the car will be at %f speed in %d at tick"%(speed, self.gameModel.getTickCounter()+50))

            canThrottle = True
            if self.gameModel.getTickCounter() > 0:
                currentPieceIndex = self.gameModel.getCurrentPieceIndex()
                if self.gameModel.isOnOptimalLane() == False and self.gameModel.isOnSwitchPiece() == False and currentPieceIndex > self.lastSwitchIndex:
                    if self.needToSwitch == False:
                        self.needToSwitch = True
                        self.nextIndexToSwitch = self.gameModel.nextSwitchPieces()-1
                        print("Marking as a need to switch when reaching %d"%(self.nextIndexToSwitch))
                    elif self.nextIndexToSwitch == currentPieceIndex:
                        print("Reached target block, trying to switch!")
                        canThrottle = False
                        self.lastSwitchIndex = currentPieceIndex
                elif self.gameModel.isOnOptimalLane() == True:
                    nextSwitch = self.gameModel.nextSwitchPieces()
                    if self.gameModel.laneStillOptimalAfterIndex(nextSwitch) == False and self.gameModel.isOnSwitchPiece() == False and currentPieceIndex > self.lastSwitchIndex:
                        if self.needToSwitch == False:
                            self.needToSwitch = True
                            self.nextIndexToSwitch = self.gameModel.nextSwitchPieces()-1
                            print("Marking as a need to switch when reaching %d (future) %d"%(self.nextIndexToSwitch, self.gameModel.getTickCounter()))
                        elif self.nextIndexToSwitch == currentPieceIndex:
                            print("Reached target block, trying to switch (future)! %d"%(self.haveThrottled))
                            canThrottle = False
                            self.lastSwitchIndex = currentPieceIndex

            #canThrottle = True #Disable lane switch, since there's still a bug in self.gameModel.calculateTrackTicks!!
            if canThrottle:
                currentThrottle = self.throttle()
                self.setThrottle(currentThrottle)
                self.haveThrottled = True
            else:
                self.switchLane(self.gameModel.switchToRight(self.lastSwitchIndex))
                self.needToSwitch = False


    def throttle(self):
        #if (self.canThrottle):
        currSpeed = self.gameModel.getCurrentCarSpeed()
        maxSpeed = 2000.0
        pieceIdx=self.gameModel.getCurrentPieceIndex()
        currPiece = self.gameModel.getTrackPiece(pieceIdx)
        nextPiece = self.gameModel.getTrackPiece(pieceIdx+1)
        throttle = 1.0 # Full speed as a default
        ownCar = self.gameModel.getCarForName(self.gameModel.ownName)
        ownAngle = ownCar.pieceAngle #ABS not used!!
        ownPos = int(self.gameModel.calculateMeterPosition(pieceIdx, ownCar.pieceLaneIdx, ownCar.inPieceDistance))

        self.cruiseCalibrate()

        #CURVE Behaviour
        if (currPiece.turnRadius != 0):
            curveSign = currPiece.originalAngle/abs(currPiece.originalAngle) #-1 or +1
            curveSlipAngle = curveSign * self.CONST_SLIPANGLE #-60 or +60
            curvature = self.gameModel.curvature(pieceIdx, ownCar.pieceLaneIdx, ownCar.inPieceDistance, 1)
                
            #CALCULATE angle changes
            angleDiff = ownAngle - self.prevAngle
            angleChangeCalc = ownAngle + angleDiff**3
            #print "angleDiff:", angleChangeCalc," angle:", ownAngle," curveSlipAngle:", curveSlipAngle, " ",
            if (curveSlipAngle<0): #Negative if angle is positive
                curveSlipAngle = -curveSlipAngle
                angleChangeCalc = -angleChangeCalc
                angleDiff = -angleDiff
            if (angleChangeCalc > curveSlipAngle): #Changing too fast towards slipangle
                #print "ANGLECHANGEBREAK ",
                throttle = 0.0
            elif (abs(angleDiff)>abs((1/curvature)/35)): #Angle changing too fast to any direction relative to lane radius (=1/laneCurvature) 
                #print "ANGLEDIFFBREAK (", angleDiff,">",abs((1/curvature)/30),") ",
                throttle = 0.0
            else:
                maxSpeed = self.gameModel.calculateMaxSpeed(curvature)
                slipAngleDiff = abs(curveSign*self.CONST_SLIPANGLE - ownAngle) #0-120, where 0 is slipangle and 120 is as far as possible from slipping
                
                if (slipAngleDiff > self.CONST_SLIPANGLE): #Angle is oppisite direction than curve
                    #print "OPPANGLE(",slipAngleDiff,") ",
                    throttle = 1.0
                elif (slipAngleDiff < self.CONST_SLIPANGLE - self.CONST_HIGHANGLE): #i.e 60 - 48 = 12 
                    #print "HIGHANGLE ",
                    if (angleDiff<0): #AngleDiff is oppisite direction than curve
                        #print "HIGHANGLE CRUISE ",
                        throttle = self.gameModel.calculateCruiseThrottle(curvature)
                    else:
                        #print "HIGHANGLE BREAK ",
                        throttle = 0.0
                elif (slipAngleDiff < self.CONST_SLIPANGLE - self.CONST_LOWANGLE): #i.e 60 - 30 = 30
                    #print "MIDANGLE ",
                    if (angleDiff<0): #AngleDiff is oppisite direction than curve
                        #print "MIDANGLE(1.0) ",
                        throttle = 1.0
                    else:
                        #print "MIDANGLE CRUISE ",
                        throttle = 1.1 * self.gameModel.calculateCruiseThrottle(curvature)
                else:
                    #print "LOWANGLE ",
                    if (angleDiff > 0.5): #Angle is changing to bad direction
                        throttle = self.gameModel.calculateCruiseThrottle(curvature)
                    else: #Angle is not changing much or changing for better
                        throttle = 1.0

        if (throttle != 0.0):
            #find next place to slow down
            pos = (ownPos+1) % self.gameModel.getMeterCount(ownCar.pieceLaneIdx)
            #Search meter pos with the next different curvature
            ownCurv = self.gameModel.getMeterCurvature(ownCar.pieceLaneIdx,ownPos)
            while (ownCurv == self.gameModel.getMeterCurvature(ownCar.pieceLaneIdx,pos)):
                pos = (pos+1) % self.gameModel.getMeterCount(ownCar.pieceLaneIdx)

            #Continue search to meter where "magic value" is bigger than abs(self.magicValueLimit) -> Curve that needs attention
            while ( (abs(self.gameModel.getMeterMagic(ownCar.pieceLaneIdx,pos))<self.magicValueLimit)
                    or (self.gameModel.getMeterCurvature(ownCar.pieceLaneIdx,pos)==0) or (pos==ownPos) ):
                pos = (pos+1) % self.gameModel.getMeterCount(ownCar.pieceLaneIdx)

            if (pos != ownPos):
                targetSpeed = 1.2 * self.gameModel.calculateMaxSpeed(self.gameModel.getMeterCurvature(ownCar.pieceLaneIdx,pos))
                averageSpeed = (currSpeed+targetSpeed)/2
                if (currSpeed > targetSpeed): #If we go faster than we should do in the next curve piece, calculate needed break distance
                    breakDist = self.gameModel.ticksToReachVelDecel(0, targetSpeed, currSpeed, 0) * averageSpeed/60 #Estimate ticks to meters using average speed
                    curveDist = pos-ownPos
                    if (curveDist < 0):
                        curveDist += self.gameModel.getMeterCount(ownCar.pieceLaneIdx)
                    if (curveDist <= breakDist):
                        #print "DISTBREAK Sp(",int(currSpeed),"->", int(targetSpeed),") oPos(",ownPos,") pos(",pos,") cDist(", int(curveDist), ") ",
                        print "DISTBREAK C:", self.gameModel.throttleModelConstantDecel, " ",
                        throttle = 0.0

        self.prevAngle = ownAngle
        print "THROTTLE:",throttle," Angle:",ownAngle
        if (throttle > 1.0):
            throttle = 1.0
        return throttle

    def calibResults(self, cns, mul, accel):
        self.accelResCns.append(cns)
        self.accelResMul.append(mul)
        if len (self.accelResCns) == len(self.accelList):
            avgMul = 0.0
            avgCns = 0.0
            cnt = len(self.accelList)
            for i in range(cnt):
                avgMul += self.accelResMul[i] / self.accelList[i]
                avgCns += self.accelResCns[i]
            avgMul /= float(cnt)
            avgCns /= float(cnt)
            self.gameModel.setThrottleModel(avgMul, avgCns)
            self.calibResFinished = True
        print("Results, C: %f, M: %f"%(cns,mul))

    def cruiseCalibrate(self):
        speed = self.gameModel.getCurrentCarSpeed()
        ownCar = self.gameModel.getCarForName(self.gameModel.ownName)
        ownAngle = ownCar.pieceAngle
        pieceIdx = self.gameModel.getCurrentPieceIndex()
        curvature = self.gameModel.curvature(pieceIdx, ownCar.pieceLaneIdx, ownCar.inPieceDistance, 1)
        angleDiff = ownAngle - self.prevAngle
        newSlipConst = 0.0

        if (speed>100) and (abs(ownAngle)>20) and (self.gameModel.getTrackPiece(pieceIdx).turnRadius != 0):
            radius = abs(1/curvature)
            if (curvature*ownAngle<0): #if curvature and angle have different signs, we are slipping
                #print "radius:",radius," ownAngle",ownAngle," prevAngle:",self.prevAngle," angleDiff:",angleDiff,""
                if (abs(angleDiff)<0.1):
                    newSlipConst = speed**2 / radius
                else:
                    maxSpeed = self.gameModel.calculateMaxSpeed(curvature)
                    #print "SPEED vs MAXSPEED ",speed,"-",maxSpeed
                    if (speed<maxSpeed) and (angleDiff > 1.0):
                        newSlipConst = (speed-(5*angleDiff))**2/radius

        if (newSlipConst > 1):
            self.cruiseCalibCount += 1
            #Add new information as balanced average to previous value
            self.gameModel.slipConstant = float(self.gameModel.slipConstant * (self.cruiseCalibCount-1) + newSlipConst) / self.cruiseCalibCount
            #print "CruiseCalib slipC:", self.gameModel.slipConstant, " newSC:",newSlipConst, " count:", self.cruiseCalibCount

    def onTurbo(self, data, extra):
        game.BaseBot.onTurbo(self, data, extra)
        print(data)
        #self.useTurbo()
        self.ping()

    def onCrash(self, data, extra):
        game.BaseBot.onCrash(self, data, extra)
        print("Someone crashed")
        self.ping()



