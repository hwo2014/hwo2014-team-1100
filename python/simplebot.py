import game

class SimpleBot(game.BaseBot):
    def __init__(self, socket, name, key):
        game.BaseBot.__init__(self, socket, name, key)
        # do personal initializations
        self.accelList = [0.456] # [0.25, 0.45] #[0.15, 0.25, 0.35, 0.45]
        self.accelResCns = []
        self.accelResMul = []
        self.accelIdx = 0
        self.accelDataCount = -1
        self.accelSampleCount = 250 # 250 ticks seems to be roughly when speed maxs out
        self.accelData = []
        self.calibrator = None
        self.accelerating = True
        self.accelCalibDone = False
        self.prevSpeed = 0.0

        self.startFinalAccel = False
        self.calibResFinished = False

    def onGameInit(self, data, extra):
        game.BaseBot.onGameInit(self, data, extra)
        self.gameModel.setOptimalLaneStrategy(game.gameDataModel.OPTIMAL_LANE_CURVATURE)

    def speedCalibAccel(self):
        carSpeed = self.gameModel.getCurrentCarSpeed() / 60.0
        if self.accelerating == True:
            currentThrottle = self.accelList[self.accelIdx]
            if carSpeed > 0.0:  # if we're moving start recording
                if self.accelDataCount < 0: #start
                    print("Started recording accel data for throttle: %f"%(currentThrottle))
                    self.accelDataCount = 2
                    self.accelData = []
                    self.accelData.append(0.0)
                    self.accelData.append(carSpeed)
                elif self.accelDataCount < self.accelSampleCount:
                    self.accelData.append(carSpeed)
                    self.accelDataCount += 1
                else:   # finished
                    print("Finished with accel data for throttle: %f"%(currentThrottle))
                    self.calibrator = game.speedCalibrator(True, self.accelData, self.calibResults)
                    self.calibrator.start()
                    self.accelDataCount = -1
                    self.accelerating = False
                    self.accelIdx += 1
                    if self.accelIdx >= len(self.accelList):
                        self.accelCalibDone = True
        else:
            currentThrottle = 0.0
            if abs(self.prevSpeed-carSpeed) < 0.001 and carSpeed < 0.01:
                print("stopped decelaration at speed: %f %f"%(carSpeed, self.prevSpeed))
                self.accelerating = True
            self.prevSpeed = carSpeed
        return currentThrottle

    def onCarPositions(self, data, extra):
        game.BaseBot.onCarPositions(self, data, extra)
        if False: #self.accelCalibDone == False:
            currentThrottle = self.speedCalibAccel()
        else:
            if self.startFinalAccel == False and self.calibResFinished == True:
                self.startFinalAccel = True
                ticks = self.gameModel.ticksRequiredToReachSpeed(0.98*1.0*self.gameModel.throttleModelMul, 1.0)
                speed = self.gameModel.speedAtTick(self.gameModel.getTickCounter()+50, 1.0)
                print("Estimating that it takes until %d ticks to reach: %f speed"%(ticks,0.98*1.0*self.gameModel.throttleModelMul*60.0))
                print("Estimating that the car will be at %f speed in %d at tick"%(speed, self.gameModel.getTickCounter()+50))
            currentThrottle = 0.4 # 1.0
        self.setThrottle(currentThrottle)

    def calibResults(self, cns, mul, accel):
        self.accelResCns.append(cns)
        self.accelResMul.append(mul)
        if len (self.accelResCns) == len(self.accelList):
            avgMul = 0.0
            avgCns = 0.0
            cnt = len(self.accelList)
            for i in range(cnt):
                avgMul += self.accelResMul[i] / self.accelList[i]
                avgCns += self.accelResCns[i]
            avgMul /= float(cnt)
            avgCns /= float(cnt)
            self.gameModel.setThrottleModel(avgMul, avgCns)
            self.calibResFinished = True
        print("Results, C: %f, M: %f"%(cns,mul))

    def onTurbo(self, data, extra):
        print(data)
        #self.useTurbo()
        self.ping()
