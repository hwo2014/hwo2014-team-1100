import math
import bgmath

PI = 3.14159265358
radToAngleMultiplier = PI/180.0

def ourAngleToMathAngle(angle):
    return 360.0-angle

def ourAngleToMathAngleRads(angle):
    return ourAngleToMathAngle(angle)*radToAngleMultiplier

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    @staticmethod
    def createUnit(angle):
        return Point(math.cos(ourAngleToMathAngleRads(angle)), math.sin(ourAngleToMathAngleRads(angle)))
    @staticmethod
    def createUnitFrom(start, end):
        xDiff = end.x-start.x
        yDiff = end.y-start.y
        vectLenInv = 1.0/math.sqrt(xDiff*xDiff+yDiff*yDiff)
        return Point(xDiff * vectLenInv, yDiff * vectLenInv)
    @staticmethod
    def distanceBetween(start, end):
        xDiff = end.x-start.x
        yDiff = end.y-start.y
        return math.sqrt(xDiff*xDiff+yDiff*yDiff)
    @staticmethod
    def normalizeVector(vect):
        lengthInv = 1.0/Point.distanceBetween(Point(0,0),vect)
        return Point(vect.x*lengthInv, vect.y*lengthInv)
    @staticmethod
    def rotateVector(vect, angle):
        cz = math.cos(ourAngleToMathAngleRads(angle))
        sz = math.sin(ourAngleToMathAngleRads(angle))
        return Point(vect.x*cz-vect.y*sz, vect.x*sz+vect.y*cz)
    def __repr__(self):
        return "(%f, %f)"%(self.x,self.y)

class LaneInfo:
    def __init__(self, index, distance):
        self.index = index          # index used to indentify lane
        self.distance = distance    # distance from piece center line
        self.length = 0             # actual length of the lane
        self.startPoint = Point(0, 0)
        self.endPoint = Point(0, 0)
        self.midPoint = Point(0, 0) # point in the middle of the lane
        self.switchDistances = {}   # distance to other lanes when switching
        self.laneRadius = 0.0       # radius of the lane if curved piece
    def setLaneSwitchLength(self, index, distance):
        self.switchDistances[index] = distance
    def getLaneSwitchLength(self, index):
        try:
            dist = self.switchDistances[index]
        except:
            dist = 0
        return dist
    def __repr__(self):
        ret = "index : %d distance : %d length : %d "%(self.index,self.distance,self.length)
        ret += "start x: %f y: %f end x: %f y: %f"%(self.startPoint.x, self.startPoint.y, self.endPoint.x, self.endPoint.y)
        ret += format(self.switchDistances)
        return ret

class TrackPiece:
    TYPE_UNKNOWN = 0
    TYPE_STRAIGHT = 1
    TYPE_CURVED = 2
    def __init__(self, pieceType):
        self.vertices = []
        self.lanes = []
        #direction to which piece is pointing to if it is straight
        #on curved its the starting angle of the arc
        self.pieceDirection = 0
        self.realAngle = 0
        #these two are only valid for curved pieces
        self.originalAngle = 0
        self.turnRadius = 0.0
        #the ending angle of the arc, on straight irrelevant
        self.finalDirection = 0
        self.length = 0
        self.midPoint = Point(0, 0)
        self.midPointEnd = Point(0, 0)
        self.arcMidPoint = Point(0, 0)
        self.type = pieceType
        self.switch = False
    def addVertice(self, point):
        self.vertices.append(point)
    def setMiddlePoint(self, point):
        self.midPoint.x = point[0]
        self.midPoint.y = point[1]
    def setEndMiddlePoint(self, point):
        self.midPointEnd.x = point[0]
        self.midPointEnd.y = point[1]
    def getVertices(self):
        return self.vertices
    def getLaneByIndex(self, index):
        for lane in self.lanes:
            if lane.index == index:
                return lane
        print("lane %d not found"%(index))
        return None
    def __repr__(self):
        ret = ''
        if self.type == self.TYPE_UNKNOWN:
            ret += 'Unknown '
        elif self.type == self.TYPE_STRAIGHT:
            ret += 'Straight '
        elif self.type == self.TYPE_CURVED:
            ret += 'Curved '
        ret += 'length: %f '%(self.length)
        for vertIdx in range(len(self.vertices)):
            vert = self.vertices[vertIdx]
            ret += 'vertex%d x: %f y: %f '%(vertIdx, vert[0], vert[1])
        ret += 'lanes '
        for lane in self.lanes:
            ret += format(lane)
        return ret

class TrackCreator:
    trackWidthPadding = 20.0
    def __init__(self, startPoint, startDirection):
        self.currentPoint = Point(startPoint[0], startPoint[1])
        self.currentTrackAngle = startDirection
        self.currentTrackWidth = 0.0

    def createStraigthPiece(self, length, laneData):
        piece = TrackPiece(TrackPiece.TYPE_STRAIGHT)
        piece.pieceDirection = self.currentTrackAngle
        piece.realAngle = self.currentTrackAngle
        piece.finalDirection = self.currentTrackAngle
        piece.length = length
        downDir = Point.createUnit(self.currentTrackAngle-90.0)
        frwdDir = Point.createUnit(self.currentTrackAngle)

        nextPoint = Point(self.currentPoint.x+frwdDir.x*length, self.currentPoint.y+frwdDir.y*length)
        downPointDiff = Point(downDir.x*self.currentTrackWidth, downDir.y*self.currentTrackWidth)

        piece.addVertice((self.currentPoint.x, self.currentPoint.y))
        piece.addVertice((nextPoint.x, nextPoint.y))
        piece.addVertice((nextPoint.x+downPointDiff.x, nextPoint.y+downPointDiff.y))
        piece.addVertice((self.currentPoint.x+downPointDiff.x, self.currentPoint.y+downPointDiff.y))

        piece.setMiddlePoint((self.currentPoint.x+downPointDiff.x/2.0, self.currentPoint.y+downPointDiff.y/2.0))
        piece.setEndMiddlePoint((nextPoint.x+downPointDiff.x/2.0, nextPoint.y+downPointDiff.y/2.0))

        # calculate lanes
        for lane in laneData:
            sideDir = Point.createUnit(self.currentTrackAngle+90.0)
            newLane = LaneInfo(int(lane['index']),float(lane['distanceFromCenter']))
            newLane.length = length
            newLane.startPoint = Point(piece.midPoint.x+(sideDir.x*newLane.distance), piece.midPoint.y+(sideDir.y*newLane.distance))
            newLane.endPoint = Point(piece.midPointEnd.x+(sideDir.x*newLane.distance), piece.midPointEnd.y+(sideDir.y*newLane.distance))
            piece.lanes.append(newLane)
        # calculate distances between lanes
        laneCount = len(piece.lanes)
        for laneIdx  in range(laneCount):
            currentLane = piece.lanes[laneIdx]
            for otherLaneIdx in range(laneCount):
                otherLane = piece.lanes[otherLaneIdx]
                if laneIdx == otherLaneIdx: # straight line
                    dist = Point.distanceBetween(currentLane.startPoint, otherLane.endPoint)
                else:                       # bezier curve
                    middlePoint1 = Point((currentLane.startPoint.x+currentLane.endPoint.x)/2.0,(currentLane.startPoint.y+currentLane.endPoint.y)/2.0)
                    middlePoint2 = Point((otherLane.startPoint.x+otherLane.endPoint.x)/2.0,(otherLane.startPoint.y+otherLane.endPoint.y)/2.0)
                    dist = bgmath.approxBezierLength((currentLane.startPoint.x, currentLane.startPoint.y),
                                                     (middlePoint1.x, middlePoint1.y),
                                                     (middlePoint2.x, middlePoint2.y),
                                                     (otherLane.endPoint.x, otherLane.endPoint.y))
                currentLane.setLaneSwitchLength(otherLaneIdx, dist)
            piece.lanes[laneIdx] = currentLane
        self.currentPoint.x = nextPoint.x
        self.currentPoint.y = nextPoint.y

        return piece

    def createArcPiece(self, radius, angle, laneData):
        piece = TrackPiece(TrackPiece.TYPE_CURVED)
        piece.pieceDirection = self.currentTrackAngle
        piece.realAngle = self.currentTrackAngle+(angle/2.0)
        piece.finalDirection = self.currentTrackAngle+angle
        piece.originalAngle = angle
        piece.turnRadius = radius
        piece.addVertice((self.currentPoint.x, self.currentPoint.y))

        twistAngle = 0.0
        addWidth = 0.0
        if angle > 0:
            realRadius = radius - (self.currentTrackWidth/2.0)
            twistAngle = 90.0
            addWidth = self.currentTrackWidth
        else:
            realRadius = radius + (self.currentTrackWidth/2.0)
            twistAngle = -90.0
            addWidth = -self.currentTrackWidth

        tempAngle = ourAngleToMathAngleRads(self.currentTrackAngle+twistAngle)
        tempAngle2 = ourAngleToMathAngleRads(self.currentTrackAngle-twistAngle)
        midPoint = Point(self.currentPoint.x+math.cos(tempAngle)*realRadius, self.currentPoint.y+math.sin(tempAngle)*realRadius)
        lastPoint = (midPoint.x+math.cos(tempAngle2)*(realRadius+addWidth), midPoint.y+math.sin(tempAngle2)*(realRadius+addWidth))

        piece.arcMidPoint.x = midPoint.x
        piece.arcMidPoint.y = midPoint.y
        tempAngle = ourAngleToMathAngleRads(self.currentTrackAngle+angle-twistAngle)
        nextPoint = (midPoint.x+math.cos(tempAngle)*realRadius, midPoint.y+math.sin(tempAngle)*realRadius)
        piece.addVertice(nextPoint)
        thirdPoint = (midPoint.x+math.cos(tempAngle)*(realRadius+addWidth), midPoint.y+math.sin(tempAngle)*(realRadius+addWidth))
        piece.addVertice(thirdPoint)
        piece.addVertice(lastPoint)
        piece.setMiddlePoint((midPoint.x+math.cos(tempAngle2)*(realRadius+addWidth/2.0), midPoint.y+math.sin(tempAngle2)*(realRadius+addWidth/2.0)))
        piece.setEndMiddlePoint((midPoint.x+math.cos(tempAngle)*(realRadius+addWidth/2.0), midPoint.y+math.sin(tempAngle)*(realRadius+addWidth/2.0)))
        self.currentTrackAngle += angle

        if piece.originalAngle > 0:     # for right hand turn, right hand lane is closer to middle
            startAngle = piece.pieceDirection - 90.0
            endAngle = piece.pieceDirection + piece.originalAngle - 90.0
        else:                           # for left hand turn, right hand lane is further from middle
            startAngle = piece.pieceDirection + 90.0
            endAngle = piece.pieceDirection + piece.originalAngle + 90.0

        midPointAngle = (startAngle+endAngle)/2.0

        # calculate lanes
        for lane in laneData:
            newLane = LaneInfo(int(lane['index']),float(lane['distanceFromCenter']))
            if piece.originalAngle > 0:     # for right hand turn, right hand lane is closer to middle
                actualRadius = piece.turnRadius - newLane.distance
            else:                           # for left hand turn, right hand lane is further from middle
                actualRadius = piece.turnRadius + newLane.distance
            newLane.length = TrackCreator.calculateArcLength(piece.turnRadius, piece.originalAngle, newLane.distance)
            newLane.laneRadius = actualRadius

            dirVect = Point.createUnit(startAngle)
            newLane.startPoint = Point(piece.arcMidPoint.x+dirVect.x*actualRadius, piece.arcMidPoint.y+dirVect.y*actualRadius)
            dirVect = Point.createUnit(endAngle)
            newLane.endPoint = Point(piece.arcMidPoint.x+dirVect.x*actualRadius, piece.arcMidPoint.y+dirVect.y*actualRadius)
            dirVect = Point.createUnit(midPointAngle)
            newLane.midPoint = Point(piece.arcMidPoint.x+dirVect.x*actualRadius, piece.arcMidPoint.y+dirVect.y*actualRadius)

            piece.lanes.append(newLane)
        # calculate distances between lanes
        laneCount = len(piece.lanes)
        for laneIdx  in range(laneCount):
            currentLane = piece.lanes[laneIdx]
            for otherLaneIdx in range(laneCount):
                otherLane = piece.lanes[otherLaneIdx]
                if laneIdx == otherLaneIdx: # straight line
                    dist = TrackCreator.calculateArcLength(piece.turnRadius, piece.originalAngle, currentLane.distance)
                else:                       # bezier curve
                    dist = bgmath.approxBezierLength((currentLane.startPoint.x, currentLane.startPoint.y),
                                                     (currentLane.midPoint.x, currentLane.midPoint.y),
                                                     (otherLane.midPoint.x, otherLane.midPoint.y),
                                                     (otherLane.endPoint.x, otherLane.endPoint.y))
                currentLane.setLaneSwitchLength(otherLaneIdx, dist)
            piece.lanes[laneIdx] = currentLane
        self.currentPoint.x = nextPoint[0]
        self.currentPoint.y = nextPoint[1]

        return piece

    @staticmethod
    def calculateArcLength(radius, angle, distance):
        if angle > 0:
            actualRadius = radius - distance
            return (2.0*PI*angle*actualRadius)/360.0
        else:
            actualRadius = radius + distance
            return (2.0*PI*-angle*actualRadius)/360.0

    #input track block from gameInit JSON msg
    def generateTrackData(self, track):
        pieces = track['pieces']
        lanes = track['lanes']
        laneData = {}

        trackMin = 1000
        trackMinIdx = -1
        trackMax = -1000
        trackMaxIdx = -1
        for lane in lanes:
            distance = lane['distanceFromCenter']
            laneIdx = int(lane['index'])
            laneData[laneIdx] = LaneInfo(laneIdx, distance)
            if distance < trackMin:
                trackMin = distance
                trackMinIdx = laneIdx
            if distance > trackMax:
                trackMax = distance
                trackMaxIdx = laneIdx

        self.currentTrackWidth = self.trackWidthPadding*2.0+trackMax-trackMin

        trackPieces = []
        for piece in pieces:
            switch = piece.has_key('switch')
            if piece.has_key('length'):
                pieceLength = float(piece['length'])
                trackPiece = self.createStraigthPiece(pieceLength, lanes)
                trackPiece.switch = switch
                trackPieces.append(trackPiece)
            else:
                pieceRadius = float(piece['radius'])
                pieceAngle = float(piece['angle'])
                trackPiece = self.createArcPiece(pieceRadius, pieceAngle, lanes)
                trackPiece.switch = switch
                trackPieces.append(trackPiece)

        return (trackPieces, laneData, (trackMinIdx, trackMaxIdx))
