import json
import socket
import socks
import sys
import visuals
import game
import argparse
import simplebot
import kekebot

if __name__ == "__main__":

    botList = {
        'keke':kekebot.KekeBot,
        'simple': simplebot.SimpleBot,
        }

    # parse args
    argParse = argparse.ArgumentParser(description='BemmiGummit visual bot run\r\nNote: in multicar games the track name, car count and password need to match.')
    argParse.add_argument('--logRecv', dest='logRecv', help='Log file to save the received gameTick/gameInit JSON messages.')
    argParse.add_argument('--server','-s', dest='server', required=True, help='Server address for connection')
    argParse.add_argument('--port','-p', dest='port', required=True, help='Server port for connection')
    argParse.add_argument('--name','-n', dest='name', required=True, help='Name of the bot to run on server')
    argParse.add_argument('--key','-k', dest='key', required=True, help='The magic key value to identify us')
    argParse.add_argument('--proxy','-x', dest='proxy', help='Proxy server address and port in address:port form')
    argParse.add_argument('--track','-t', dest='track', help='Name of the track to run', default='keimola')
    argParse.add_argument('--bot','-b', dest='bot', help='Name of the bot model to use', default='keke')
    argParse.add_argument('--cars','-c', dest='cars', help='Number of cars', default=1, type=int)
    argParse.add_argument('--password','-w', dest='password', help='Password for the race', default=None)
    argParse.add_argument('--join','-j', dest='join', help='Join a race', action='store_true')
    argParse.add_argument('--no-visuals', dest='noVisuals', help='Do not make a visual race', action='store_true')
    argParse.add_argument('--simple-join', '-sj', dest='simpleJoin', help='Use simple join instead of joinRace or createRace', action='store_true')
    args = argParse.parse_args()

    host = args.server
    port = args.port
    name = args.name
    key = args.key
    proxy = args.proxy

    print("Connecting with parameters:")
    print("host={%s}, port={%s}, bot name={%s}, key={%s}"%(host,port,name,key))

    if proxy == None:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
    else:
        proxyserver, proxyport = proxy.split(":")
        s = socks.socksocket()
        s.setproxy(socks.PROXY_TYPE_HTTP, proxyserver, int(proxyport))
        s.connect((host, int(port)))

    #gameModel.setLogFileGen(args.logRecv)

    try:
        botObject = botList[args.bot](s, name, key)
    except:
        print("Couldn't create object for bot : %s, using default (simple)"%(args.bot))
        botObject = botList['keke'](s, name, key)
    botObject.setNumberOfCars(args.cars)
    botObject.setGamePassword(args.password)
    botObject.setTrack(args.track)
    if args.simpleJoin == True:
        botObject.useSimpleJoin()
    else:
        botObject.setCreateJoin(args.join != True)

    gameModel = botObject.getGameModel()
    if args.noVisuals == False:
        visuals = visuals.gameVisuals(1000, gameModel)
        botObject.setBotStopCallback(visuals.stopGraphics)
    else:
        print("Not using any visualizations!")

    if args.logRecv != None:
        gameModel.setLogFileRecv(args.logRecv)

    botObject.start()
    if args.noVisuals == False:
        visuals.run()
    print("Graphics died, waiting for Bot to finish!")
    botObject.waitUntilBotDone()
    print("Bot is finished!")

    if args.logRecv != None:
        gameModel.closeLogRecv()
    #gameModel.closeLogGen()
