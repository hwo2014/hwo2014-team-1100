try:
    import pygame
except ImportError:
    print("couldn't import pygame")

import track
import game
import argparse

class gameVisuals(object):
    BLACK = ( 0, 0, 0)
    WHITE = ( 255, 255, 255)
    BLUE = ( 0, 0, 255)
    GREEN = ( 0, 255, 0)
    RED = ( 255, 0, 0)
    GREY = ( 128, 128, 128 )
    YELLOW = (255, 242, 0)
    screenXPadding = 50
    screenYPadding = 50
    progressBarPadding = 20.0
    sliderButtonHeight = 20.0
    sliderButtonWidth = 40.0

    def __init__(self, minDimens, game):
        pygame.init()
        pygame.display.set_caption("BemmiGummit visuals")
        self.clock = pygame.time.Clock()

        self.game = game
        #defaults
        self.screenWidth = minDimens
        self.screenHeight = minDimens
        #self.carPositions = None
        self.done = False

        # for non-connected visualization
        self.fileTicks = None
        self.replay = False

    def calculateScreenDimensions(self, trackScale):
        # calculate the size of the screen shown based on minimum requested dimensions
        ratio = trackScale.x / trackScale.y
        if ratio < 0:   # portrait
            self.screenWidth = int(self.screenHeight*ratio)
        else:           # landscape
            self.screenHeight = int(self.screenWidth/ratio)
        self.size = (self.screenWidth, self.screenHeight)
        # calculate scaling factors used to scale data to fit the display
        self.screenScale = ((self.screenWidth -self.screenXPadding*2)/trackScale.x,
                            (self.screenHeight-self.screenYPadding*2)/trackScale.y)

    def stopGraphics(self):
        self.done = True

    def scalePoint(self, point):
        return (self.screenXPadding+((point[0]-self.trackDimensions[0].x)*self.screenScale[0]),
            self.screenHeight-(self.screenYPadding+((point[1]-self.trackDimensions[0].y)*self.screenScale[1])))

    def getQuadraticBezierPoint(self, p0, p1, p2, p3, t):
        tm = (1.0-t)
        return track.Point(tm*tm*tm*p0.x+3*tm*tm*t*p1.x+3*tm*t*t*p2.x+t*t*t*p3.x,
            tm*tm*tm*p0.y+3*tm*tm*t*p1.y+3*tm*t*t*p2.y+t*t*t*p3.y)

    def drawScaledBezier(self, color, p0, p1, p2, p3):
        sp0 = self.scalePoint(p0)
        sp1 = self.scalePoint(p1)
        sp2 = self.scalePoint(p2)
        sp3 = self.scalePoint(p3)
        t = 0
        lineCount = 19.0
        prev = self.getQuadraticBezierPoint(track.Point(sp0[0], sp0[1]),
                                            track.Point(sp1[0], sp1[1]),
                                            track.Point(sp2[0], sp2[1]),
                                            track.Point(sp3[0], sp3[1]), t/lineCount)
        for t in range(1,int(lineCount+1)):
            newP = self.getQuadraticBezierPoint(track.Point(sp0[0], sp0[1]),
                                                track.Point(sp1[0], sp1[1]),
                                                track.Point(sp2[0], sp2[1]),
                                                track.Point(sp3[0], sp3[1]), t/lineCount)
            pygame.draw.line(self.screen, color, (prev.x, prev.y), (newP.x, newP.y), 1)
            prev = newP

    def drawScaledLine(self, color, startpoint, endpoint, width):
        start = self.scalePoint(startpoint)
        end = self.scalePoint(endpoint)
        pygame.draw.line(self.screen, color, start,end, 1)

    def drawScaledPolygon(self, color, vertices, width):
        scaledVertices = []
        for vertice in vertices:
            scaledVertices.append(self.scalePoint(vertice))
        pygame.draw.polygon(self.screen, color, scaledVertices, width)

    def drawScaledPoint(self, color, point, width):
        scaledPoint = self.scalePoint(point)
        pygame.draw.rect(self.screen, color, [scaledPoint[0]-width/2,scaledPoint[1]-width/2,width,width], width)

    def drawScaledArc(self, color, box, startangle, endangle):
        boxPoint = self.scalePoint((box[0], box[1]))
        boxFinal = [boxPoint[0], boxPoint[1], box[2] * self.screenScale[0], box[3] *self.screenScale[1]]
        if startangle < endangle:
            pygame.draw.arc(self.screen, color, boxFinal, track.ourAngleToMathAngleRads(endangle), track.ourAngleToMathAngleRads(startangle))
        else:
            pygame.draw.arc(self.screen, color, boxFinal, track.ourAngleToMathAngleRads(startangle), track.ourAngleToMathAngleRads(endangle))

    def drawScaledRect(self, color, box, width):
        boxPoint = self.scalePoint((box[0], box[1]))
        boxFinal = [boxPoint[0], boxPoint[1], box[2] * self.screenScale[0], box[3] *self.screenScale[1]]
        pygame.draw.rect(self.screen, color, boxFinal, width)

    def drawTrack(self, screen):
        trackData = self.game.getTrackData()
        optimalLanes = self.game.getOptimalLanes()
        trackWidth = self.game.getTrackWidth()
        for pieceIdx in range(len(trackData)):
            piece = trackData[pieceIdx]
            # first draw the piece
            if piece.type == track.TrackPiece.TYPE_STRAIGHT:
                self.drawScaledPolygon(self.BLUE, piece.getVertices(), 1)
            elif piece.type == track.TrackPiece.TYPE_CURVED:
                if piece.originalAngle > 0:
                    startAngle = piece.pieceDirection-90.0
                else:
                    startAngle = piece.pieceDirection+90.0
                endAngle = startAngle + piece.originalAngle
                minRad = piece.turnRadius - (trackWidth/2)
                maxRad = piece.turnRadius + (trackWidth/2)
                box=[piece.arcMidPoint.x -minRad, piece.arcMidPoint.y +minRad, minRad*2, minRad*2]
                self.drawScaledArc(self.BLUE, box, startAngle, endAngle)
                box=[piece.arcMidPoint.x -maxRad, piece.arcMidPoint.y +maxRad, maxRad*2, maxRad*2]
                self.drawScaledArc(self.BLUE, box, startAngle, endAngle)
                verts = piece.getVertices()
                self.drawScaledLine(self.BLUE, verts[0], verts[3], 1)
                self.drawScaledLine(self.BLUE, verts[1], verts[2], 1)
            # then draw the lanes
            sideDir = track.Point.createUnit(piece.pieceDirection+90.0)
            sideDirEnd = track.Point.createUnit(piece.finalDirection+90.0)
            laneCount = len(piece.lanes)
            for laneIdx in range(laneCount):
                lane = piece.lanes[laneIdx]
                if piece.switch:
                    if laneIdx == 0:                # first track
                        otherLane = piece.getLaneByIndex(lane.index+1)
                        theOtherLane = None
                    elif laneIdx == (laneCount-1):  # last track
                        otherLane = piece.getLaneByIndex(lane.index-1)
                        theOtherLane = None
                    else:                           # any track
                        otherLane = piece.getLaneByIndex(lane.index+1)
                        theOtherLane = piece.getLaneByIndex(lane.index-1)
                    if theOtherLane != None:
                        if lane.index == optimalLanes[pieceIdx-1] and otherLane.index == optimalLanes[pieceIdx+1]:
                            color = self.RED
                        elif lane.index == optimalLanes[pieceIdx-1] and theOtherLane.index == optimalLanes[pieceIdx+1]:
                            color = self.RED
                        else:
                            color = self.GREY
                    else:   # side track, compare to other
                        if lane.index == optimalLanes[pieceIdx-1] and otherLane.index == optimalLanes[pieceIdx+1]:
                            color = self.RED
                        else:
                            color = self.GREY
                    if piece.type == track.TrackPiece.TYPE_STRAIGHT:
                        self.drawScaledBezier(color, (lane.startPoint.x, lane.startPoint.y),
                            ((lane.startPoint.x+lane.endPoint.x)/2, (lane.startPoint.y+lane.endPoint.y)/2),
                            ((otherLane.startPoint.x+otherLane.endPoint.x)/2, (otherLane.startPoint.y+otherLane.endPoint.y)/2),
                            (otherLane.endPoint.x, otherLane.endPoint.y))
                        if theOtherLane != None:
                            self.drawScaledBezier(color, (lane.startPoint.x, lane.startPoint.y),
                                ((lane.startPoint.x+lane.endPoint.x)/2, (lane.startPoint.y+lane.endPoint.y)/2),
                                ((theOtherLane.startPoint.x+theOtherLane.endPoint.x)/2, (theOtherLane.startPoint.y+theOtherLane.endPoint.y)/2),
                                (theOtherLane.endPoint.x, theOtherLane.endPoint.y))
                    elif piece.type == track.TrackPiece.TYPE_CURVED:
                        self.drawScaledBezier(color, (lane.startPoint.x, lane.startPoint.y),
                                         (lane.midPoint.x, lane.midPoint.y),
                                         (otherLane.midPoint.x, otherLane.midPoint.y),
                                         (otherLane.endPoint.x, otherLane.endPoint.y))
                        if theOtherLane != None:
                            self.drawScaledBezier(color, (lane.startPoint.x, lane.startPoint.y),
                                             (lane.midPoint.x, lane.midPoint.y),
                                             (theOtherLane.midPoint.x, theOtherLane.midPoint.y),
                                             (theOtherLane.endPoint.x, theOtherLane.endPoint.y))
                if piece.switch:
                    if lane.index == optimalLanes[pieceIdx-1] and lane.index == optimalLanes[pieceIdx+1]:
                        pieceColor = self.RED
                    else:
                        pieceColor = self.GREY
                else:
                    if lane.index == optimalLanes[pieceIdx]:
                        pieceColor = self.RED
                    else:
                        pieceColor = self.GREY
                if piece.type == track.TrackPiece.TYPE_STRAIGHT:
                    self.drawScaledLine(pieceColor, (lane.startPoint.x, lane.startPoint.y),  (lane.endPoint.x, lane.endPoint.y), 1)
                elif piece.type == track.TrackPiece.TYPE_CURVED:
                    if piece.originalAngle > 0:
                        minRad = piece.turnRadius - lane.distance
                    else:
                        minRad = piece.turnRadius + lane.distance
                    box=[piece.arcMidPoint.x -minRad, piece.arcMidPoint.y +minRad, minRad*2, minRad*2]
                    self.drawScaledArc(pieceColor, box, startAngle, endAngle)

    def drawCar(self, car, idx, font):
        piece = self.game.getTrackPiece(car.pieceIndex)

        text = font.render("Car '%s' lap : %d angle : %.3f lane : %d spd.: %.3f m/s"%(car.name,car.pieceLap,car.pieceAngle,car.pieceLaneIdx, car.currentSpeed), True, self.BLACK)
        self.screen.blit(text, [20, 75+idx*25])

        if car.directionVector != None:
            dirVect = track.Point.normalizeVector(car.directionVector)
            carDim = car.createCarPiece((car.curPos.x, car.curPos.y), car.pieceAngle, dirVect)
        else:
            carDim = car.createCarPiece((car.curPos.x, car.curPos.y), car.currentTurnAngle+car.pieceAngle)
            dirVect = track.Point.createUnit(car.currentTurnAngle)
        # draw frame of the car
        self.drawScaledPolygon(self.BLUE, carDim.getVertices(), 1)
        # draw a vector indicating the direction the car is moving (slippage angle is indicated by the cars frame)
        self.drawScaledLine(self.BLACK, [car.curPos.x, car.curPos.y], [car.curPos.x+(dirVect.x*20), car.curPos.y+(dirVect.y*20)], 1)
        # draw a box where the guideflag is positioned
        self.drawScaledPoint(self.GREEN, [car.curPos.x, car.curPos.y], 3)
        # draw a box where the middle point of the car is
        if car.midPoint != None:
            self.drawScaledPoint(self.RED, [car.midPoint.x, car.midPoint.y], 3)

    def drawTickBar(self):
        if self.fileTicks != None:
            start = (self.progressBarPadding, self.size[1]-self.progressBarPadding)
            end = (self.size[0]-self.progressBarPadding, self.size[1]-self.progressBarPadding)
            pygame.draw.line(self.screen, self.GREY, start,end, 1)

            locX = (float(self.currentTick)/float(self.numTicks))*float(self.size[0]-self.progressBarPadding*2)
            self.sliderPoint = [self.progressBarPadding+locX, self.size[1]-self.progressBarPadding]
            pygame.draw.rect(self.screen, self.RED, [self.sliderPoint[0]-(self.sliderButtonWidth/2.0),
                                                     self.sliderPoint[1]-(self.sliderButtonHeight/2.0),
                                                     self.sliderButtonWidth, self.sliderButtonHeight], 1)

    def insideBox(self, point, box):
        halfWidth = (box[2]/2.0)
        halfHeight = (box[3]/2.0)
        if point[0] > (box[0]-halfWidth) and point[0] < (box[0]+halfWidth) and point[1] > (box[1]-halfHeight) and point[1] < (box[1]+halfHeight):
            return True
        else:
            return False

    def run(self):
        self.numTicks = 0
        self.currentTick = 0

        if self.fileTicks != None:
            self.numTicks = len(self.fileTicks)
            tick = self.fileTicks[self.currentTick]
            self.game.updateGameTickInfo(tick['data'], tick['gameTick'])

        mouseDragging = False
        self.mouseLoc = [0,0]
        self.sliderPoint = [0,0]
        print("Waiting for track data to init display!")
        self.game.checkForDataReady()
        self.trackDimensions = self.game.getTrackDimensions()
        self.calculateScreenDimensions(self.trackDimensions[2])
        print("Starting graphics loop!")
        self.screen = pygame.display.set_mode(self.size)
        # Loop as long as done == False
        while not self.done:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.done = True
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        mx = event.pos[0]
                        my = event.pos[1]
                        if self.insideBox((mx, my), (self.sliderPoint[0], self.sliderPoint[1], self.sliderButtonWidth, self.sliderButtonHeight)):
                            mouseDragging = True
                        self.mouseLoc = event.pos
                elif event.type == pygame.MOUSEBUTTONUP:
                    if event.button == 1:
                        mouseDragging = False
                        #self.mouseLoc = event.pos
                elif event.type == pygame.MOUSEMOTION and mouseDragging == True:
                    self.mouseLoc = event.pos

                if mouseDragging == True:
                    mx = self.mouseLoc[0]

                    mx -= self.progressBarPadding
                    mx /= (self.size[0]-self.progressBarPadding*2)
                    prevTick = self.currentTick
                    self.currentTick = int(mx*self.numTicks)
                    if self.currentTick < 0:
                        self.currentTick = 0
                    elif self.currentTick >= self.numTicks:
                        self.currentTick = self.numTicks-1
                    if prevTick != self.currentTick:
                        tick = self.fileTicks[self.currentTick]
                        self.game.updateGameTickInfo(tick['data'], tick['gameTick'])

            if self.replay:
                tick = self.fileTicks[self.currentTick]
                self.game.updateGameTickInfo(tick['data'], tick['gameTick'])
                self.currentTick += 1
                if self.currentTick >= self.numTicks:
                    self.replay = False

            self.screen.fill(self.WHITE)
            self.drawTrack(self.screen)
            font = pygame.font.Font(None, 20)
            idx = 0
            for car in self.game.getCarData():
                self.drawCar(car, idx, font)
                idx += 1
            text = font.render("Game tick : %d, time : %.3f s"%(self.game.getTickCounter(),self.game.getTickCounter()/60.0), True, self.BLACK)

            self.drawTickBar()

            self.screen.blit(text, [20, 50])
            pygame.display.flip()
            self.clock.tick(50)
        pygame.quit()

    def loadGameTicksFromFile(self, fileName):
        dataFile = open(fileName, "rt")
        lines = dataFile.read().splitlines()
        dataFile.close()

        self.fileTicks = []
        self.game.initTrackData(eval(lines[0]))
        lineCount = len(lines)
        print("Loading %d lines from %s"%(lineCount, fileName))
        for lineIdx in range(1,lineCount):
            line = lines[lineIdx]
            self.fileTicks.append(eval(line))

if __name__ == "__main__":
    # parse args
    argParse = argparse.ArgumentParser(description='BemmiGummit visualizer')
    argParse.add_argument('--logRecv', required=True, dest='logRecv', help='Log file of the received gameTick JSON messages.')
    argParse.add_argument('--convert', '-c', dest='convert', help='car and log file of the converted data from recorded log')
    args = argParse.parse_args()

    # init
    gameModel = game.gameDataModel()
    visuals = gameVisuals(1000, gameModel)

    # load
    if args.logRecv != None:
        visuals.loadGameTicksFromFile(args.logRecv)
    else:
        print("File to load track data missing!")

    if args.convert != None:
        carName, logFile = args.convert.split(":")
        gameModel.setOwnCarInfo(carName, None)
        gameModel.setLogFileGen(logFile)
        visuals.replay = True

    #gameModel.setOptimalLaneStrategy(game.gameDataModel.OPTIMAL_LANE_CURVATURE)
    gameModel.setOptimalLaneStrategy(game.gameDataModel.OPTIMAL_LANE_DISTANCE)

    #data = [0.09999999999999996, 0.19799999999999995, 0.29404000000000025, 0.3881591999999999, 0.48039601600000004, 0.57078809568, 0.6593723337663994, 0.7461848870910726, 0.8312611893492502, 0.9146359655622642, 0.9963432462510206, 1.0764163813259982, 1.1548880536994814, 1.231790292625489, 1.307154486772983, 1.3810113970375186, 1.4533911690967685, 1.5243233457148386, 1.5938368788005342, 1.6619601412245244, 1.7287209384000417, 1.7941465196320343, 1.8582635892393937, 1.921098317454605, 1.9826763511055185, 2.0430228240834025, 2.1021623676017334, 2.1601191202497043, 2.2169167378447105, 2.2725784030878184, 2.327126835026047, 2.380584298325543, 2.432972612359029, 2.4843131601118342, 2.534626896909613, 2.5839343589714177, 2.6322556717919756, 2.679610558356152, 2.726018347189032, 2.7714979802452375, 2.8160680206403503, 2.8597466602275357, 2.902551727022968, 2.9445006924825274, 2.9856106786328733, 3.025898465060195, 3.0653804957590225, 3.10407288584382, 3.141991428126964, 3.1791515995644275, 3.2155685675731145, 3.251257196221666, 3.2862320522972444, 3.3205074112512696, 3.354097263026255, 3.387015317765752, 3.4192750114104045, 3.450889511182219, 3.48187172095855, 3.5122342865394285, 3.541989600808604, 3.571149808792411, 3.599726812616614, 3.6277322763642537, 3.6551776308369432, 3.682074078220273, 3.7084325966558085, 3.7342639447226817, 3.759578665828292, 3.78438709251169, 3.8086993506614246, 3.8325253636482506, 3.855874856375277, 3.8787573592477256, 3.901182212062828, 3.923158567821542, 3.94469539646509, 3.965801488535853, 3.9864854587650984, 4.0067557495897965, 4.02662063459804, 4.0460882219059995, 4.06516645746792, 4.083863128318578, 4.102185865752186, 4.120142148437121, 4.137739305468424, 4.154984519359042, 4.171884828971807, 4.188447132392478, 4.204678189744536, 4.220584625949678, 4.236172933430718, 4.251449474762049, 4.266420485266773, 4.281092075561483, 4.295470234050282, 4.309560829369255, 4.3233696127818835, 4.336902220526253]
    #gameModel.calculateAccelModelConstants(104,4.350164,0.500000,data,0)
    #a = [4.263160892593399, 4.177897674741454, 4.094339721246721, 4.012452926821747, 3.9322038682853235, 3.8535597909196677, 3.7764885951012106, 3.70095882319917, 3.6269396467352855, 3.5544008538004572, 3.4833128367244925, 3.4136465799900604, 3.345373648390235, 3.2784661754223943, 3.212896851913997, 3.1486389148756615, 3.0856661365781264, 3.0239528138466474, 2.9634737575696457, 2.904204282418304, 2.846120196769908, 2.7891977928345257, 2.733413836977856, 2.6787455602382204, 2.6251706490334876, 2.572667236052917, 2.5212138913316946, 2.470789613505212, 2.421373821235044, 2.372946344810273, 2.325487417914154, 2.278977669555864, 2.2333981161647376, 2.188730153841411, 2.1449555507646405, 2.1020248861682025, 2.0599852077430176, 2.018786671828402, 1.9784120379309778, 1.9388448320500837, 1.9000689094278547, 1.8620684479783873, 1.824827941846488, 1.788332195096238, 1.752566315524223, 1.717515708594991, 1.6831660714990346, 1.6495033873276297, 1.6165139193642746, 1.5841842054878008, 1.5525010526914917, 1.5214515317056265, 1.4910229717316654, 1.461202955278434, 1.4319793131038527, 1.4033401192537307, 1.3752736862036687, 1.347768560094949, 1.3208135160658854, 1.294397553676967, 1.2685098924270268, 1.2431399673582038, 1.218277424749005, 1.1939121178940117, 1.1700341029656582, 1.146633634961192, 1.1237011637288497, 1.1012273300730893, 1.0792029619394556, 1.0576190706732271, 1.0364668473542167, 1.0157376592042815, 0.9954230460669923, 0.9755147169564452, 0.9560045466767432, 0.9368845725069362, 0.9181469909537945, 0.8997841545690068, 0.8817885688291162, 0.8641528890788904, 0.8468699175354754, 0.8299326003513486, 0.8133340247376896, 0.7970674161437744, 0.7811261354933705, 0.765503676476301, 0.7501936628939505, 0.7351898460578298, 0.7204861022405094, 0.7060764301763601, 0.6919549486144714, 0.6781158939173006, 0.6645536177103293, 0.6512625845770148, 0.6382373697996064, 0.6254726571470727, 0.6129632367045239, 0.6007040027474905, 0.5886899516599065]
    #gameModel.calculateDecelModelConstants(104,204,4.350164,0.590449,0.000000,a)

    visuals.run()
